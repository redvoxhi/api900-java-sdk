### RedVox Java SDK

This repository contains code for reading and working with the RedVox API 900 data format from Java.

See: https://bitbucket.org/redvoxhi/api900-java-sdk/src/master/docs/0.2/redvox-api900-java-docs.md for SDK documentation.

### Changelog

##### 0.2

* Update API documentation
* Add more tests
* First release on maven central

##### 0.1

* Initial release


