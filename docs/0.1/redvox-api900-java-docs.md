## Redvox API 900 SDK Documentation

The RedVox Java SDK is written in Java 1.8+ and provides utility classes, methods, and functions for working with RedVox API 900 data. The API 900 standalone reader provides functionality for easily accessing all packet fields and sensor payloads within individual API 900 packets. The API 900 reader does not perform queries over multiple data sets or aggregation of data.

The Redvox API 900 utilizes Google's protobuf library for serializing and deserializing data between devices. It's possible to interact with API 900 data directly by selecting a pre-compiled language wrapper at https://bitbucket.org/redvoxhi/redvox-data-apis/src/master/src/api900/generated_code/ and including the wrapper in your software project. These wrappers provide all the functionality required for creating and reading Redvox API 900 files. The low level API 900 data format is described and documented in detail at: https://bitbucket.org/redvoxhi/redvox-data-apis/src/master/doc/api900/api900.md?at=master&fileviewer=file-view-default

### Table of Contents

* [Prerequisites](#markdown-header-prerequisites)
* [Installing from Maven](#markdown-header-installing-from-maven)
* [Loading RedVox API 900 files](#markdown-header-loading-redvox-api-900-files)
* [Working with microphone sensor channels](#markdown-header-working-with-microphone-sensor-channels)
* [Working with barometer sensor channels](#markdown-header-working-with-barometer-sensor-channels)
* [Working with location sensor channels](#markdown-header-working-with-location-sensor-channels)
* [working with time synchronization channels](#markdown-header-working-with-time-synchronization-sensor-channels)
* [Working with accelerometer sensor channels](#markdown-header-working-with-accelerometer-sensor-channels)
* [Working with magnetometer sensor channels](#markdown-header-working-with-magenetometer-sensor-channels)
* [Working with gyroscope sensor channels](#markdown-header-working-with-gyroscope-sensor-channels)
* [Working with light sensor channels](#markdown-header-working-with-light-sensor-channels)
* [Working with infrafred sensor channels](#markdown-header-working-with-infrared-sensor-channels)
* [Full Example](https://bitbucket.org/redvoxhi/api900-java-sdk/raw/master/src/test/java/examples/Example_0_1.java)
* [Generated API Documentation](https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/index.html)

### Prerequisites

Java 1.8 or greater is required. 

This project depends on protobuf-java, protobuf-java-util, and lz4-java libraries.

### Installing from Jar
The latest fat jar with all dependencies ready to include in your project can be found at: https://bitbucket.org/redvoxhi/api900-java-sdk/downloads/

### Installing from Maven

TODO

### Installing from source

However, if you are looking for the source distribution, it can be found at https://bitbucket.org/redvoxhi/api900-java-sdk/downloads/

### Loading RedVox API 900 Files

The class `io.redvox.api900.Reader` contains two functions for loading RedVox API 900 data files: `readBuffer` and `readFile`. The module also contains one function, `wrap`, that wraps the low-level protobuf RedVox packet in our high-level API which allows easy access to packet fields and sensor data.

`readBuffer` accepts an array of bytes which represent a serialized RedVox data packet and returns a Optional of a low-level protobuf `Api900.RedvoxPacket`. `readFile` accepts the path to a RedVox data packet file stored somewhere on the file system and also returns an Optional of a low-level protobuf `Api900.RedvoxPacket`.

We can call the `wrap` method to wrap a low-level protobuf packet in our high-level API.

The full documentation for the reader can be found at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/Reader.html

A `WrappedRedvoxPacket` is a class that acts as a wrapper around the raw protobuf API and provides convenience methods for accessing fields and sensor channels of the loaded RedVox packet.

Once a packet is wrapped in WrappedRedvoxPacket class, high-level accessors can be used to access both top level data fields as well as instances of sensor objects. See the API documentation on WrappedRedvoxPacket for a listing of all available methods and descriptions: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/WrappedRedvoxPacket.html


##### Example loading RedVox data

Let's look at loading a file and accessing some of top level fields. 

```
// First, read in protobuf encoded RedVox file
final Optional<Api900.RedvoxPacket> redvoxPacketOptional = Reader.readFile("/home/data/0000001314_1539627249223.rdvxz");

// If we successfully loaded the protobuf encoded message, we can now wrap it to access high-level accessors.
if(redvoxPacketOptional.isPresent()) {
    final WrappedRedvoxPacket wrappedRedvoxPacket = Reader.wrap(redvoxPacketOptional.get());

    // Print contents of top-level data fields
    System.out.println(wrappedRedvoxPacket.api());
    System.out.println(wrappedRedvoxPacket.redvoxId());
    System.out.println(wrappedRedvoxPacket.uuid());
    System.out.println(wrappedRedvoxPacket.authenticatedEmail());
    System.out.println(wrappedRedvoxPacket.authenticationToken());
    System.out.println(wrappedRedvoxPacket.firebaseToken());
    System.out.println(wrappedRedvoxPacket.isBackfilled());
    System.out.println(wrappedRedvoxPacket.isPrivate());
    System.out.println(wrappedRedvoxPacket.isScrambled());
    System.out.println(wrappedRedvoxPacket.deviceMake());
    System.out.println(wrappedRedvoxPacket.deviceModel());
    System.out.println(wrappedRedvoxPacket.deviceOs());
    System.out.println(wrappedRedvoxPacket.deviceOsVersion());
    System.out.println(wrappedRedvoxPacket.appVersion());
    System.out.println(wrappedRedvoxPacket.batteryLevelPercent());
    System.out.println(wrappedRedvoxPacket.deviceTemperatureC());
    System.out.println(wrappedRedvoxPacket.acquisitionServer());
    System.out.println(wrappedRedvoxPacket.timeSynchronizationChannel());
    System.out.println(wrappedRedvoxPacket.authenticationServer());
    System.out.println(wrappedRedvoxPacket.appFileStartTimestampEpochMicrosecondsUtc());
    System.out.println(wrappedRedvoxPacket.appFileStartTimestampMachine());
    System.out.println(wrappedRedvoxPacket.serverTimestampEpochMicrosecondsUtc());
    System.out.println(wrappedRedvoxPacket.metadataList());
    System.out.println(wrappedRedvoxPacket.metadataMap());
}
```


### Working with microphone sensor channels
It's possible to test for the availability of this sensor in a data packet by calling the method `hasMicrophoneChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `microphoneChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `MicrophoneSensor` class contains methods for directly accessing the fields and payloads of microphone channels. The API provides a full listing of methods and descriptions from the microphone channel: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/MicrophoneSensor.html

##### Example microphone sensor reading

```
// Retrieve and print contents of microphone sensor channel
if(wrappedRedvoxPacket.hasMicrophoneChannel()){
    final MicrophoneSensor microphoneSensor = wrappedRedvoxPacket.microphoneChannel().get();
    System.out.println(microphoneSensor.sensorName());
    System.out.println(microphoneSensor.sampleRateHz());
    System.out.println(microphoneSensor.firstSampleTimestampEpochMicrosecondsUtc());
    System.out.println(microphoneSensor.payloadType());
    System.out.println(microphoneSensor.payloadValues());
    System.out.println(microphoneSensor.payloadMean());
    System.out.println(microphoneSensor.payloadMedian());
    System.out.println(microphoneSensor.payloadStd());
    System.out.println(microphoneSensor.metadataList());
    System.out.println(microphoneSensor.metadataMap());
}
```

### Working with barometer sensor channels

It's possible to test for the availability of this sensor in a data packet by calling the method `hasBarometerChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `barometerChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `BarometerSensor` class contains methods for directly accessing the fields and payloads of barometer channels. The full listing of methods and descriptions for the barometer sensor can be found at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/BarometerSensor.html

##### Example barometer sensor reading

```
// Retrieve and print contents of barometer sensor channel
if(wrappedRedvoxPacket.hasBarometerChannel()) {
    final BarometerSensor barometerSensor = wrappedRedvoxPacket.barometerChannel().get();
    System.out.println(barometerSensor.sensorName());
    System.out.println(barometerSensor.timestampsMicrosecondsUtc());
    System.out.println(barometerSensor.sampleIntervalMean());
    System.out.println(barometerSensor.sampleIntervalMedian());
    System.out.println(barometerSensor.sampleIntervalStd());
    System.out.println(barometerSensor.payloadValues());
    System.out.println(barometerSensor.payloadMean());
    System.out.println(barometerSensor.payloadMedian());
    System.out.println(barometerSensor.payloadStd());
    System.out.println(barometerSensor.metadataList());
    System.out.println(barometerSensor.metadataMap());
}
```

### Working with location sensor channels

It's possible to test for the availability of this sensor in a data packet by calling the method `hasLocationChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `locationChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `LocationSensor` class contains methods for directly accessing the fields and payloads of location channels. The location channel can return the payload as interleaved values or also return the individual components of the payload. A full listing of LocationSensor methods and descriptions is available at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/LocationSensor.html

##### Example locations sensor reading

```
// Retrieve and print contents of location sensor channel
if(wrappedRedvoxPacket.hasLocationChannel()) {
    final LocationSensor locationSensor = wrappedRedvoxPacket.locationChannel().get();
    System.out.println(locationSensor.sensorName());
    System.out.println(locationSensor.timestampsMicrosecondsUtc());
    System.out.println(locationSensor.sampleIntervalMean());
    System.out.println(locationSensor.sampleIntervalMedian());
    System.out.println(locationSensor.sampleIntervalStd());
    System.out.println(locationSensor.metadataList());
    System.out.println(locationSensor.metadataMap());

    System.out.println(locationSensor.payloadType());
    System.out.println(locationSensor.payloadValues());

    System.out.println(locationSensor.payloadValuesLatitude());
    System.out.println(locationSensor.payloadValuesLatitudeMean());
    System.out.println(locationSensor.payloadValuesLatitudeMedian());
    System.out.println(locationSensor.payloadValuesLatitudeStd());

    System.out.println(locationSensor.payloadValuesLongitude());
    System.out.println(locationSensor.payloadValuesLongitudeMean());
    System.out.println(locationSensor.payloadValuesLongitudeMedian());
    System.out.println(locationSensor.payloadValuesLongitudeStd());

    System.out.println(locationSensor.payloadValuesAltitude());
    System.out.println(locationSensor.payloadValuesAltitudeMean());
    System.out.println(locationSensor.payloadValuesAltitudeMedian());
    System.out.println(locationSensor.payloadValuesAltitudeStd());

    System.out.println(locationSensor.payloadValuesSpeed());
    System.out.println(locationSensor.payloadValuesSpeedMean());
    System.out.println(locationSensor.payloadValuesSpeedMedian());
    System.out.println(locationSensor.payloadValuesSpeedStd());

    System.out.println(locationSensor.payloadValuesAccuracy());
    System.out.println(locationSensor.payloadValuesAccuracyMean());
    System.out.println(locationSensor.payloadValuesAccuracyMedian());
    System.out.println(locationSensor.payloadValuesAccuracyStd());
}
```

### Working with time synchronization sensor channels

It's possible to test for the availability of this sensor in a data packet by calling the method `hasTimeSynchronizationChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `timeSynchronizationChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `TimeSynchronizationSensor` class contains methods for directly accessing the fields and payloads of time synchronization channels. A full listing of methods and descriptions for this channel can be found at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/TimeSynchronizationSensor.html


##### Example time synchronization sensor reading

```
// Retrieve and print contents of time synchronization channel
if(wrappedRedvoxPacket.hasTimeSynchronizationChannel()) {
    final TimeSynchronizationSensor timeSynchronizationSensor = wrappedRedvoxPacket.timeSynchronizationChannel().get();
    System.out.println(timeSynchronizationSensor.payloadType());
    System.out.println(timeSynchronizationSensor.payloadValues());
    System.out.println(timeSynchronizationSensor.metadataList());
    System.out.println(timeSynchronizationSensor.metadataMap());
}
```

### Working with accelerometer sensor channels

It's possible to test for the availability of this sensor in a data packet by calling the method `hasAccelerometerChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `accelerometerChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `AccelerationSensor` class contains methods for directly accessing the fields and payloads of accelerometer channels. The accelerometer sensor payload can either be accessed as a single interleaved payload which contains all X, Y, and Z components, or each component can be accessed individually. The full listing of methods and descriptions for this channel is available at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/AccelerometerSensor.html


##### Example accelerometer sensor reading

```
// Retrieve and print contents of accelerometer channel
if(wrappedRedvoxPacket.hasAccelerometerChannel()) {
    final AccelerometerSensor accelerometerSensor = wrappedRedvoxPacket.accelerometerChannel().get();
    System.out.println(accelerometerSensor.sensorName());
    System.out.println(accelerometerSensor.timestampsMicrosecondsUtc());
    System.out.println(accelerometerSensor.sampleIntervalMean());
    System.out.println(accelerometerSensor.sampleIntervalMedian());
    System.out.println(accelerometerSensor.sampleIntervalStd());
    System.out.println(accelerometerSensor.metadataList());
    System.out.println(accelerometerSensor.metadataMap());
    System.out.println(accelerometerSensor.payloadType());
    System.out.println(accelerometerSensor.payloadValues());
    System.out.println(accelerometerSensor.payloadValuesX());
    System.out.println(accelerometerSensor.payloadValuesY());
    System.out.println(accelerometerSensor.payloadValuesZ());
    System.out.println(accelerometerSensor.payloadValuesXMean());
    System.out.println(accelerometerSensor.payloadValuesXMedian());
    System.out.println(accelerometerSensor.payloadValuesXStd());
    System.out.println(accelerometerSensor.payloadValuesYMean());
    System.out.println(accelerometerSensor.payloadValuesYMedian());
    System.out.println(accelerometerSensor.payloadValuesYStd());
    System.out.println(accelerometerSensor.payloadValuesZMean());
    System.out.println(accelerometerSensor.payloadValuesZMedian());
    System.out.println(accelerometerSensor.payloadValuesZStd());
}
```

### Working with magnetometer sensor channels

It's possible to test for the availability of this sensor in a data packet by calling the method `hasMagnetometerChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `magnetometerChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `MagnetometerSensor` class contains methods for directly accessing the fields and payloads of magnetometer channels. The magnetometer sensor payload can either be accessed as a single interleaved payload which contains all X, Y, and Z components, or each component can be accessed individually. The full listing of methods and descriptions associated with this channel is available at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/MagnetometerSensor.html

##### Example magnetometer sensor reading

```
// Retrieve and print contents of magnetometer channel
if(wrappedRedvoxPacket.hasMagnetometerChannel()) {
    final MagnetometerSensor magnetometerSensor = wrappedRedvoxPacket.magnetometerChannel().get();
    System.out.println(magnetometerSensor.sensorName());
    System.out.println(magnetometerSensor.timestampsMicrosecondsUtc());
    System.out.println(magnetometerSensor.sampleIntervalMean());
    System.out.println(magnetometerSensor.sampleIntervalMedian());
    System.out.println(magnetometerSensor.sampleIntervalStd());
    System.out.println(magnetometerSensor.metadataList());
    System.out.println(magnetometerSensor.metadataMap());
    System.out.println(magnetometerSensor.payloadType());
    System.out.println(magnetometerSensor.payloadValues());
    System.out.println(magnetometerSensor.payloadValuesX());
    System.out.println(magnetometerSensor.payloadValuesY());
    System.out.println(magnetometerSensor.payloadValuesZ());
    System.out.println(magnetometerSensor.payloadValuesXMean());
    System.out.println(magnetometerSensor.payloadValuesXMedian());
    System.out.println(magnetometerSensor.payloadValuesXStd());
    System.out.println(magnetometerSensor.payloadValuesYMean());
    System.out.println(magnetometerSensor.payloadValuesYMedian());
    System.out.println(magnetometerSensor.payloadValuesYStd());
    System.out.println(magnetometerSensor.payloadValuesZMean());
    System.out.println(magnetometerSensor.payloadValuesZMedian());
    System.out.println(magnetometerSensor.payloadValuesZStd());
}
```

### Working with gyroscope sensor channels

It's possible to test for the availability of this sensor in a data packet by calling the method `hasGyroscopeChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `gyroscopeChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `GyroscopeSensor` class contains methods for directly accessing the fields and payloads of gyroscope channels. The gyroscope sensor payload can either be accessed as a single interleaved payload which contains all X, Y, and Z components, or each component can be accessed individually. A full listing of methods and descriptions for this sensor can be found at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/GyroscopeSensor.html

##### Example gyroscope sensor reading

```
// Retrieve and print contents of gyroscope channel
if(wrappedRedvoxPacket.hasGyroscopeChannel()) {
    final GyroscopeSensor gyroscopeSensor = wrappedRedvoxPacket.gyroscopeChannel().get();
    System.out.println(gyroscopeSensor.sensorName());
    System.out.println(gyroscopeSensor.timestampsMicrosecondsUtc());
    System.out.println(gyroscopeSensor.sampleIntervalMean());
    System.out.println(gyroscopeSensor.sampleIntervalMedian());
    System.out.println(gyroscopeSensor.sampleIntervalStd());
    System.out.println(gyroscopeSensor.metadataList());
    System.out.println(gyroscopeSensor.metadataMap());
    System.out.println(gyroscopeSensor.payloadType());
    System.out.println(gyroscopeSensor.payloadValues());
    System.out.println(gyroscopeSensor.payloadValuesX());
    System.out.println(gyroscopeSensor.payloadValuesY());
    System.out.println(gyroscopeSensor.payloadValuesZ());
    System.out.println(gyroscopeSensor.payloadValuesXMean());
    System.out.println(gyroscopeSensor.payloadValuesXMedian());
    System.out.println(gyroscopeSensor.payloadValuesXStd());
    System.out.println(gyroscopeSensor.payloadValuesYMean());
    System.out.println(gyroscopeSensor.payloadValuesYMedian());
    System.out.println(gyroscopeSensor.payloadValuesYStd());
    System.out.println(gyroscopeSensor.payloadValuesZMean());
    System.out.println(gyroscopeSensor.payloadValuesZMedian());
    System.out.println(gyroscopeSensor.payloadValuesZStd());
}
```

### Working with light sensor channels

It's possible to test for the availability of this sensor in a data packet by calling the method `hasLightChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `lightChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `LightSensor` class contains methods for directly accessing the fields and payloads of light channels. A full listing of methods and descriptions for this sensor channel can be found at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/LightSensor.html

##### Example light sensor reading

```
// Retrieve and print contents of light sensor channel
if(wrappedRedvoxPacket.hasLightChannel()) {
    final LightSensor lightSensor = wrappedRedvoxPacket.lightChannel().get();
    System.out.println(lightSensor.sensorName());
    System.out.println(lightSensor.timestampsMicrosecondsUtc());
    System.out.println(lightSensor.sampleIntervalMean());
    System.out.println(lightSensor.sampleIntervalMedian());
    System.out.println(lightSensor.sampleIntervalStd());
    System.out.println(lightSensor.payloadType());
    System.out.println(lightSensor.payloadValues());
    System.out.println(lightSensor.payloadMean());
    System.out.println(lightSensor.payloadMedian());
    System.out.println(lightSensor.payloadStd());
    System.out.println(lightSensor.metadataList());
    System.out.println(lightSensor.metadataMap());
}
```

### Working with infrared sensor channels

It's possible to test for the availability of this sensor in a data packet by calling the method `hasInfraredChannel` on an instance of a `WrappedRedvoxPacket`.

The sensor can be accessed from an instance of a `WrappedRedvoxPacket` by calling the method `infraredChannel`. An empty Optional is returned if the data packet does not have a channel of this sensor type.

The `InfraredSensor` class contains methods for directly accessing the fields and payloads of infrared channels. A full listing of methods and descriptions available for this sensor channel can be found at: https://redvoxhi.bitbucket.io/redvox-sdk-java/0.1/api/io/redvox/api900/sensors/InfraredSensor.html

##### Example infrared sensor reading

```
// Retrieve and print contents of infrared sensor channel
if(wrappedRedvoxPacket.hasInfraredChannel()) {
    final InfraredSensor infraredSensor = wrappedRedvoxPacket.infraredChannel().get();
    System.out.println(infraredSensor.sensorName());
    System.out.println(infraredSensor.timestampsMicrosecondsUtc());
    System.out.println(infraredSensor.sampleIntervalMean());
    System.out.println(infraredSensor.sampleIntervalMedian());
    System.out.println(infraredSensor.sampleIntervalStd());
    System.out.println(infraredSensor.payloadType());
    System.out.println(infraredSensor.payloadValues());
    System.out.println(infraredSensor.payloadMean());
    System.out.println(infraredSensor.payloadMedian());
    System.out.println(infraredSensor.payloadStd());
    System.out.println(infraredSensor.metadataList());
    System.out.println(infraredSensor.metadataMap());
}
```