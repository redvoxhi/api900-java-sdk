package io.redvox.api900;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.redvox.apis.Api900;

/**
 * This class contains functions for converting a redvoxPacket into JSON.
 */
public class Writer {
    /**
     * Converts a redvoxPacket into a JSON representation.
     * @param redvoxPacket The packet to convert to JSON.
     * @return A string containing the JSON of the converted RedVox packet.
     */
    public static String writeJson(final Api900.RedvoxPacket redvoxPacket) {
        try {
            return JsonFormat.printer().print(redvoxPacket);
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return "";
        }
    }
}
