package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

/**
 * This class represents an accelerometer sensor channel with X, Y, and Z channels.
 */
public class AccelerometerSensor extends XyzUnevenlySampledSensor<Double> {
    public AccelerometerSensor(Api900.UnevenlySampledChannel unevenlySampledChannel) {
        super(unevenlySampledChannel,
                PayloadUtil::extractFloatingPayload,
                Api900.ChannelType.ACCELEROMETER_X,
                Api900.ChannelType.ACCELEROMETER_Y,
                Api900.ChannelType.ACCELEROMETER_Z);
    }
}
