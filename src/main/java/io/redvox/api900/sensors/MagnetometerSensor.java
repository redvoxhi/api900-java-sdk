package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

/**
 * This class represents an magnetometer sensor channel with X, Y, and Z channels.
 */
public class MagnetometerSensor extends XyzUnevenlySampledSensor<Double> {
    public MagnetometerSensor(Api900.UnevenlySampledChannel unevenlySampledChannel) {
        super(unevenlySampledChannel,
                PayloadUtil::extractFloatingPayload,
                Api900.ChannelType.MAGNETOMETER_X,
                Api900.ChannelType.MAGNETOMETER_Y,
                Api900.ChannelType.MAGNETOMETER_Z);
    }
}
