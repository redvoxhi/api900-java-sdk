package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ImageSensor extends UnevenlySampledSensor<Byte> {
    final private List<Integer> imageOffsets;

    public ImageSensor(Api900.UnevenlySampledChannel unevenlySampledChannel) {
        super(unevenlySampledChannel, PayloadUtil::extractBytePayload);
        this.imageOffsets = parseOffsets();
    }

    private List<Integer> parseOffsets() {
        final Map<String, String> metadata = this.metadataMap();
        if(metadata.containsKey("images")) {
            final String offsetsLine = metadata.get("images").replace("[", "").replace("]", "");
            try {
                return Arrays.stream(offsetsLine.split(",")).map(Integer::parseInt).collect(Collectors.toList());
            } catch(NumberFormatException e) {
                return Collections.emptyList();
            }
        }
        return Collections.emptyList();
    }

    public byte[] payloadBytes() {
        return this.unevenlySampledChannel.getBytePayload().getPayload().toByteArray();
    }

    public int numImages() {
        return this.imageOffsets.size();
    }

    public List<Integer> getImageOffsets() {
        return this.imageOffsets;
    }

    public byte[] getImageBytes(final int idx) {
        if(idx < 0 || idx >= this.numImages()) {
            throw new IllegalArgumentException(String.format("idx must be between 0 and the total number of images available in this packet %d", this.numImages()));
        }

        final byte[] original = this.payloadBytes();
        final int offset = this.imageOffsets.get(idx);

        byte[] imageBytes;
        if(idx == this.numImages() - 1) {
            imageBytes = new byte[original.length - this.imageOffsets.get(idx)];
        }
        else {
            imageBytes = new byte[imageOffsets.get(idx + 1) - offset];
        }

        System.arraycopy(original, offset, imageBytes, 0, imageBytes.length);
        return imageBytes;
    }

    public void writeImageToFile(final int idx, final String path) throws IOException {
        Files.write(Paths.get(path), getImageBytes(idx));
    }

    public String defaultFilename(final int idx) {
        return String.format("%d.jpg", this.timestampsMicrosecondsUtc().get(idx));
    }

    public void writeAllImagesToDirectory() throws IOException {
        this.writeAllImagesToDirectory(".");
    }

    public void writeAllImagesToDirectory(final String directory) throws IOException {
        for(int i = 0; i < this.numImages(); i++) {
            this.writeImageToFile(i, String.format("%s/%s", directory, this.defaultFilename(i)));
        }
    }
}
