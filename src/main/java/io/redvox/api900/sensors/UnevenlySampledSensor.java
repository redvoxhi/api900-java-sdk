package io.redvox.api900.sensors;

import io.redvox.api900.PayloadFunction;
import io.redvox.api900.Util;
import io.redvox.apis.Api900;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents the base class for all unevenly sampled channels.
 * @param <R> The payload data type.
 */
public class UnevenlySampledSensor<R extends Number & Comparable<R>> {
    private final PayloadFunction<Api900.UnevenlySampledChannel, List<R>> transformFn;
    private final Map<Api900.ChannelType, Integer> channelIndexCache;
    private final int totalChannels;

    /**
     * Reference to original protobuf encoded unevenly sampled channel.
     */
    public final Api900.UnevenlySampledChannel unevenlySampledChannel;

    UnevenlySampledSensor(final Api900.UnevenlySampledChannel unevenlySampledChannel,
                          final PayloadFunction<Api900.UnevenlySampledChannel, List<R>> transformFn) {
        this.unevenlySampledChannel = unevenlySampledChannel;
        this.transformFn = transformFn;
        this.channelIndexCache = new HashMap<>();
        this.totalChannels = this.unevenlySampledChannel.getChannelTypesCount();
        for(int i = 0; i < this.totalChannels; i++) {
            this.channelIndexCache.put(this.unevenlySampledChannel.getChannelTypes(i), i);
        }
    }

    List<R> payloadFromChannel(final Api900.ChannelType channelType) {
        return this.transformFn.apply(this.unevenlySampledChannel,
                this.channelIndexCache.get(channelType),
                this.totalChannels);
    }

    double meanFromChannel(final Api900.ChannelType channelType) {
        return this.unevenlySampledChannel.getValueMeans(this.channelIndexCache.get(channelType));
    }

    double medianFromChannel(final Api900.ChannelType channelType) {
        return this.unevenlySampledChannel.getValueMedians(this.channelIndexCache.get(channelType));
    }

    double stdFromChannel(final Api900.ChannelType channelType) {
        return this.unevenlySampledChannel.getValueStds(this.channelIndexCache.get(channelType));
    }

    /**
     * Returns the name of the sensor.
     * @return the name of the sensor.
     */
    public String sensorName() {
        return this.unevenlySampledChannel.getSensorName();
    }

    /**
     * Returns the type of payload string as defined in the packet and by protobuf.
     * @return the type of payload string as defined in the packet and by protobuf.
     */
    public String payloadType() {
        return this.unevenlySampledChannel.getPayloadCase().name();
    }

    /**
     * Returns the list of timestamps associated with each sample (possibly interleaved).
     * @return the list of timestamps associated with each sample (possibly interleaved).
     */
    public List<Long> timestampsMicrosecondsUtc() {
        return this.unevenlySampledChannel.getTimestampsMicrosecondsUtcList();
    }

    /**
     * Returns the mean sample interval.
     * @return the mean sample interval.
     */
    public double sampleIntervalMean() {
        return this.unevenlySampledChannel.getSampleIntervalMean();
    }

    /**
     * Returns the median sample interval.
     * @return the median sample interval.
     */
    public double sampleIntervalMedian() {
        return this.unevenlySampledChannel.getSampleIntervalMedian();
    }

    /**
     * Returns the stddev of the sample interval.
     * @return the stddev of the sample interval.
     */
    public double sampleIntervalStd() {
        return this.unevenlySampledChannel.getSampleIntervalStd();
    }

    /**
     * Returns metadata added to this sensor channel as a least.
     * @return metadata added to this sensor channel as a least.
     */
    public List<String> metadataList() {
        return this.unevenlySampledChannel.getMetadataList();
    }

    /**
     * Returns metadata associated with this sensor channel as key-value pairs.
     * @return metadata associated with this sensor channel as key-value pairs.
     */
    public Map<String, String> metadataMap() {
        return Util.listToKeyPair(this.metadataList());
    }

    /**
     * Returns list of sensor channel payload values.
     * @return list of sensor channel payload values.
     */
    public List<R> payloadValues() {
        return this.transformFn.apply(unevenlySampledChannel, 0, 1);
    }

    /**
     * Mean of payload values.
     * Is calculated locally if not present.
     * @return Mean of payload values.
     */
    public double payloadMean() {
        if(this.unevenlySampledChannel.getValueMeansCount() > 0) {
            return this.unevenlySampledChannel.getValueMeans(0);
        }
        return Util.mean(this.payloadValues());
    }

    /**
     * Median of payload values.
     * Is calculated locally if not present.
     * @return Median of payload values.
     */
    public double payloadMedian() {
        if(this.unevenlySampledChannel.getValueMediansCount() > 0) {
            return this.unevenlySampledChannel.getValueMedians(0);
        }
        else {
            return Util.median(this.payloadValues());
        }
    }

    /**
     * Stddev of payload values.
     * Is calculated locally if not present.
     * @return Stddev of payload values.
     */
    public double payloadStd() {
        if(this.unevenlySampledChannel.getValueStdsCount() > 0) {
            return this.unevenlySampledChannel.getValueStds(0);
        }
        else {
            return Util.stddev(this.payloadValues());
        }
    }

    @Override
    public String toString() {
        return this.unevenlySampledChannel.toString();
    }
}
