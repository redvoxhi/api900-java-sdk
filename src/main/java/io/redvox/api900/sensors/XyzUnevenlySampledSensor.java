package io.redvox.api900.sensors;

import io.redvox.api900.PayloadFunction;
import io.redvox.apis.Api900;

import java.util.List;

/**
 * This class forms the base class for all sensors that store their data as a X, Y, and Z channels. This class inherits
 * functionality from the unevenly sampled sensor base class.
 * @param <R> The data type of payloads.
 */
public class XyzUnevenlySampledSensor<R extends Number & Comparable<R>> extends UnevenlySampledSensor<R> {
    private final Api900.ChannelType channelTypeX;
    private final Api900.ChannelType channelTypeY;
    private final Api900.ChannelType channelTypeZ;

    XyzUnevenlySampledSensor(Api900.UnevenlySampledChannel unevenlySampledChannel,
                             PayloadFunction<Api900.UnevenlySampledChannel, List<R>> transformFn,
                             Api900.ChannelType channelTypeX,
                             Api900.ChannelType channelTypeY,
                             Api900.ChannelType channelTypeZ) {
        super(unevenlySampledChannel, transformFn);
        this.channelTypeX = channelTypeX;
        this.channelTypeY = channelTypeY;
        this.channelTypeZ = channelTypeZ;
    }

    /**
     * Returns the X channel's payload values.
     * @return the X channel's payload values.
     */
    public List<R> payloadValuesX() {
        return this.payloadFromChannel(this.channelTypeX);
    }

    /**
     * Returns the Y channel's payload values.
     * @return the Y channel's payload values.
     */
    public List<R> payloadValuesY() {
        return this.payloadFromChannel(this.channelTypeY);
    }

    /**
     * Returns the Z channel's payload values.
     * @return the Z channel's payload values.
     */
    public List<R> payloadValuesZ() {
        return this.payloadFromChannel(this.channelTypeZ);
    }

    /**
     * Returns the X channel's mean payload.
     * @return the X channel's mean payload.
     */
    public double payloadValuesXMean() {
        return this.meanFromChannel(this.channelTypeX);
    }

    /**
     * Returns the Y channel's mean payload.
     * @return the Y channel's mean payload.
     */
    public double payloadValuesYMean() {
        return this.meanFromChannel(this.channelTypeY);
    }

    /**
     * Returns the Z channel's mean payload.
     * @return the Z channel's mean payload.
     */
    public double payloadValuesZMean() {
        return this.meanFromChannel(this.channelTypeZ);
    }

    /**
     * Returns the X channel's median payload.
     * @return the X channel's median payload.
     */
    public double payloadValuesXMedian() {
        return this.medianFromChannel(this.channelTypeX);
    }

    /**
     * Returns the Y channel's median payload.
     * @return the Y channel's median payload.
     */
    public double payloadValuesYMedian() {
        return this.medianFromChannel(this.channelTypeY);
    }

    /**
     * Returns the Z channel's median payload.
     * @return the Z channel's median payload.
     */
    public double payloadValuesZMedian() {
        return this.medianFromChannel(this.channelTypeZ);
    }

    /**
     * Returns the X channel's stddev payload.
     * @return the X channel's stddev payload.
     */
    public double payloadValuesXStd() {
        return this.stdFromChannel(this.channelTypeX);
    }

    /**
     * Returns the Y channel's stddev payload.
     * @return the Y channel's stddev payload.
     */
    public double payloadValuesYStd() {
        return this.stdFromChannel(this.channelTypeY);
    }

    /**
     * Returns the Z channel's stddev payload.
     * @return the Z channel's stddev payload.
     */
    public double payloadValuesZStd() {
        return this.stdFromChannel(this.channelTypeZ);
    }
}
