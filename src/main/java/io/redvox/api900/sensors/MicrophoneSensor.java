package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

/**
 * This class represents a microphone evenly sampled sensor channel.
 */
public class MicrophoneSensor extends EvenlySampledSensor<Long> {
    public MicrophoneSensor(Api900.EvenlySampledChannel evenlySampledChannel) {
        super(evenlySampledChannel, PayloadUtil::extractIntegralPayload);
    }
}
