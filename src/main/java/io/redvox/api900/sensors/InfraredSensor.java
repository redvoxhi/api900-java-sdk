package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

/**
 * This class represents a infrared unevenly sampled sensor channel.
 */
public class InfraredSensor extends UnevenlySampledSensor<Double> {
    public InfraredSensor(Api900.UnevenlySampledChannel unevenlySampledChannel) {
        super(unevenlySampledChannel, PayloadUtil::extractFloatingPayload);
    }
}
