package io.redvox.api900.sensors;


import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

/**
 * This class represents a barometer unevenly sampled sensor channel.
 */
public class BarometerSensor extends UnevenlySampledSensor<Double> {
    public BarometerSensor(Api900.UnevenlySampledChannel unevenlySampledChannel) {
        super(unevenlySampledChannel, PayloadUtil::extractFloatingPayload);
    }
}
