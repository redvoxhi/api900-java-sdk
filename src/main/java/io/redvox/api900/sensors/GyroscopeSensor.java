package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

/**
 * This class represents an gyroscope sensor channel with X, Y, and Z channels.
 */
public class GyroscopeSensor extends XyzUnevenlySampledSensor<Double> {
    public GyroscopeSensor(Api900.UnevenlySampledChannel unevenlySampledChannel) {
        super(unevenlySampledChannel,
                PayloadUtil::extractFloatingPayload,
                Api900.ChannelType.GYROSCOPE_X,
                Api900.ChannelType.GYROSCOPE_Y,
                Api900.ChannelType.GYROSCOPE_Z);
    }
}
