package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

import java.util.List;

/**
 * This class represents a location unevenly sampled sensor channel.
 */
public class LocationSensor extends UnevenlySampledSensor<Double> {
    public LocationSensor(Api900.UnevenlySampledChannel unevenlySampledChannel) {
        super(unevenlySampledChannel, PayloadUtil::extractFloatingPayload);
    }

    /**
     * Returns the latitude payload of this sensor channel.
     * @return the latitude payload of this sensor channel.
     */
    public List<Double> payloadValuesLatitude() {
        return this.payloadFromChannel(Api900.ChannelType.LATITUDE);
    }

    /**
     * Returns the longitude payload of this sensor channel.
     * @return the longitude payload of this sensor channel.
     */
    public List<Double> payloadValuesLongitude() {
        return this.payloadFromChannel(Api900.ChannelType.LONGITUDE);
    }

    /**
     * Returns the altitude payload of this sensor channel.
     * @return the altitude payload of this sensor channel.
     */
    public List<Double> payloadValuesAltitude() {
        return this.payloadFromChannel(Api900.ChannelType.ALTITUDE);
    }

    /**
     * Returns the speed payload of this sensor channel.
     * @return the speed payload of this sensor channel.
     */
    public List<Double> payloadValuesSpeed() {
        return this.payloadFromChannel(Api900.ChannelType.SPEED);
    }

    /**
     * Returns the accuracy payload of this sensor channel.
     * @return the accuracy payload of this sensor channel.
     */
    public List<Double> payloadValuesAccuracy() {
        return this.payloadFromChannel(Api900.ChannelType.ACCURACY);
    }

    /**
     * Returns the mean latitude.
     * @return the mean latitude.
     */
    public double payloadValuesLatitudeMean() {
        return this.meanFromChannel(Api900.ChannelType.LATITUDE);
    }

    /**
     * Returns the median latitude.
     * @return the median latitude.
     */
    public double payloadValuesLatitudeMedian() {
        return this.medianFromChannel(Api900.ChannelType.LATITUDE);
    }

    /**
     * Returns the stddev latitude.
     * @return the stddev latitude.
     */
    public double payloadValuesLatitudeStd() {
        return this.stdFromChannel(Api900.ChannelType.LATITUDE);
    }

    /**
     * Returns the mean longitude.
     * @return the mean longitude.
     */
    public double payloadValuesLongitudeMean() {
        return this.meanFromChannel(Api900.ChannelType.LONGITUDE);
    }

    /**
     * Returns the median longitude.
     * @return the median longitude.
     */
    public double payloadValuesLongitudeMedian() {
        return this.medianFromChannel(Api900.ChannelType.LONGITUDE);
    }

    /**
     * Returns the stddev longitude.
     * @return the stddev longitude.
     */
    public double payloadValuesLongitudeStd() {
        return this.stdFromChannel(Api900.ChannelType.LONGITUDE);
    }

    /**
     * Returns the mean altitude.
     * @return the mean altitude.
     */
    public double payloadValuesAltitudeMean() {
        return this.meanFromChannel(Api900.ChannelType.ALTITUDE);
    }

    /**
     * Returns the median altitude.
     * @return the median altitude.
     */
    public double payloadValuesAltitudeMedian() {
        return this.medianFromChannel(Api900.ChannelType.ALTITUDE);
    }

    /**
     * Returns the stddev altitude.
     * @return the stddev altitude.
     */
    public double payloadValuesAltitudeStd() {
        return this.stdFromChannel(Api900.ChannelType.ALTITUDE);
    }

    /**
     * Returns the mean speed.
     * @return the mean speed.
     */
    public double payloadValuesSpeedMean() {
        return this.meanFromChannel(Api900.ChannelType.SPEED);
    }

    /**
     * Returns the median speed.
     * @return the median speed.
     */
    public double payloadValuesSpeedMedian() {
        return this.medianFromChannel(Api900.ChannelType.SPEED);
    }

    /**
     * Returns the stddev speed.
     * @return the stddev speed.
     */
    public double payloadValuesSpeedStd() {
        return this.stdFromChannel(Api900.ChannelType.SPEED);
    }

    /**
     * Returns the mean accuracy.
     * @return the mean accuracy.
     */
    public double payloadValuesAccuracyMean() {
        return this.meanFromChannel(Api900.ChannelType.ACCURACY);
    }

    /**
     * Returns the median speed.
     * @return the median speed.
     */
    public double payloadValuesAccuracyMedian() {
        return this.medianFromChannel(Api900.ChannelType.ACCURACY);
    }

    /**
     * Returns the stddev speed.
     * @return the stddev speed.
     */
    public double payloadValuesAccuracyStd() {
        return this.stdFromChannel(Api900.ChannelType.ACCURACY);
    }
}
