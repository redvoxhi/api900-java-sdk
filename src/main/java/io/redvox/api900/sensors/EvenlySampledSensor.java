package io.redvox.api900.sensors;

import io.redvox.api900.Util;
import io.redvox.apis.Api900;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * This class forms the base class of all evenly sampled channels.
 * @param <R> The data type of payload values.
 */
public class EvenlySampledSensor<R extends Number & Comparable<R>> {
    private final Function<Api900.EvenlySampledChannel, List<R>> transformFn;

    /**
     * Reference to original protobuf encoded evenely sampled channel.
     */
    public final Api900.EvenlySampledChannel evenlySampledChannel;

    EvenlySampledSensor(final Api900.EvenlySampledChannel evenlySampledChannel,
                        final Function<Api900.EvenlySampledChannel, List<R>> transformFn) {
        this.evenlySampledChannel = evenlySampledChannel;
        this.transformFn = transformFn;
    }

    /**
     * Returns the sample rate in hz.
     * @return the sample rate in hz.
     */
    public double sampleRateHz() {
        return this.evenlySampledChannel.getSampleRateHz();
    }

    /**
     * Returns the timestamp of the first sample as microseconds since the epoch UTC.
     * @return the timestamp of the first sample as microseconds since the epoch UTC.
     */
    public long firstSampleTimestampEpochMicrosecondsUtc() {
        return this.evenlySampledChannel.getFirstSampleTimestampEpochMicrosecondsUtc();
    }

    /**
     * Returns the name of the sensor.
     * @return the name of the sensor.
     */
    public String sensorName() {
        return this.evenlySampledChannel.getSensorName();
    }

    /**
     * Returns the payload type as a protobuf encoded string.
     * @return the payload type as a protobuf encoded string.
     */
    public String payloadType() {
        return this.evenlySampledChannel.getPayloadCase().name();
    }

    /**
     * Returns any metadata associated with this sensor channel as a list.
     * @return any metadata associated with this sensor channel as a list.
     */
    public List<String> metadataList() {
        return this.evenlySampledChannel.getMetadataList();
    }

    /**
     * Returns any metadata associated with this sensor channel as key-value pairs.
     * @return any metadata associated with this sensor channel as key-value pairs.
     */
    public Map<String, String> metadataMap() {
        return Util.listToKeyPair(this.metadataList());
    }

    /**
     * Returns a list of values representing this channel's data payload.
     * @return a list of values representing this channel's data payload.
     */
    public List<R> payloadValues() {
        return this.transformFn.apply(evenlySampledChannel);
    }

    /**
     * Returns the mean of the payload, or computes it if it is not present.
     * @return the mean of the payload, or computes it if it is not present.
     */
    public double payloadMean() {
        if(this.evenlySampledChannel.getValueMeansCount() > 0) {
            return this.evenlySampledChannel.getValueMeans(0);
        }
        return Util.mean(this.payloadValues());
    }

    /**
     * Returns the median of the payload, or computes it if it is not present.
     * @return the median of the payload, or computes it if it is not present.
     */
    public double payloadMedian() {
        if(this.evenlySampledChannel.getValueMediansCount() > 0) {
            return this.evenlySampledChannel.getValueMedians(0);
        }
        else {
            return Util.median(this.payloadValues());
        }
    }

    /**
     * Returns the stddev of the payload, or computes it if it is not present.
     * @return the stddev of the payload, or computes it if it is not present.
     */
    public double payloadStd() {
        if(this.evenlySampledChannel.getValueStdsCount() > 0) {
            return this.evenlySampledChannel.getValueStds(0);
        }
        else {
            return Util.stddev(this.payloadValues());
        }
    }

    @Override
    public String toString() {
        return this.evenlySampledChannel.toString();
    }
}
