package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.api900.Util;
import io.redvox.apis.Api900;

import java.util.List;
import java.util.Map;

/**
 * This class represents a time synchronization unevenly sampled sensor channel.
 * Unlike other unevenly sampled channels, this class only contains a payload values array.
 */
public class TimeSynchronizationSensor {
    /**
     * Reference to original protobuf unevenly sampled channel.
     */
    public final Api900.UnevenlySampledChannel unevenlySampledChannel;

    public TimeSynchronizationSensor(final Api900.UnevenlySampledChannel unevenlySampledChannel) {
        this.unevenlySampledChannel = unevenlySampledChannel;
    }

    /**
     * String representation of protobuf data type.
     * @return String representation of protobuf data type.
     */
    public String payloadType() {
        return this.unevenlySampledChannel.getPayloadCase().name();
    }

    /**
     * Returns a list of payload values for this sensor channel.
     * @return a list of payload values for this sensor channel.
     */
    public List<Long> payloadValues() {
        return PayloadUtil.extractIntegralPayload(this.unevenlySampledChannel);
    }

    /**
     * Returns a list of any metadata associated with this data channel.
     * @return a list of any metadata associated with this data channel.
     */
    public List<String> metadataList() {
        return this.unevenlySampledChannel.getMetadataList();
    }

    /**
     * Returns a key-value map of any metadata associated with this data channel.
     * @return a key-value map of any metadata associated with this data channel.
     */
    public Map<String, String> metadataMap() {
        return Util.listToKeyPair(this.metadataList());
    }
}
