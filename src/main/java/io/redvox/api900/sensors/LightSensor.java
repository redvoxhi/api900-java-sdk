package io.redvox.api900.sensors;

import io.redvox.api900.PayloadUtil;
import io.redvox.apis.Api900;

/**
 * This class represents a light unevenly sampled sensor channel.
 */
public class LightSensor extends UnevenlySampledSensor<Double> {
    public LightSensor(Api900.UnevenlySampledChannel unevenlySampledChannel) {
        super(unevenlySampledChannel, PayloadUtil::extractFloatingPayload);
    }
}
