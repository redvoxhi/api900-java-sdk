package io.redvox.api900;

import net.jpountz.lz4.LZ4Compressor;
import net.jpountz.lz4.LZ4Factory;
import net.jpountz.lz4.LZ4FastDecompressor;
import net.jpountz.lz4.LZ4SafeDecompressor;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

/**
 * This class provides methods for compressing and decompressing byte arrays using Lz4.
 */
public class Lz4 {
    /**
     * This factory provides instances of LZ4 compressors and decompressors.
     */
    private static LZ4Factory lz4Factory = null;

    /**
     * An instance of an Lz4 high compressor which prioritizes higher compression ratios over speed.
     */
    private static LZ4Compressor lz4HighCompressor = null;

    /**
     * An instance of an Lz4 fast compress which prioritizes faster compression over compression ratio.
     */
    private static LZ4Compressor lz4FastCompressor = null;

    /**
     * An instance of an Lz4 fast decompressor which prioritizes fast decompression over memory safety.
     */
    private static LZ4FastDecompressor lz4FastDecompressor = null;

    /**
     * An instane of an Lz4 safe decompressor which prioritizes safe decompression over decompression speed.
     */
    private static LZ4SafeDecompressor lz4SafeDecompressor = null;


    /**
     * Returns a singleton instance of an Lz4 factory.
     * @return A singleton instance of an Lz4 factory.
     */
    public static synchronized LZ4Factory getLz4Factory() {
        if(lz4Factory == null) {
            lz4Factory = LZ4Factory.fastestInstance();
        }
        return lz4Factory;
    }

    /**
     * Returns a singleton instance of an Lz4 high compressor.
     * @return A singleton instance of an Lz4 high compressor.
     */
    public static synchronized LZ4Compressor getLz4HighCompressor() {
        if(lz4HighCompressor == null) {
            lz4HighCompressor = getLz4Factory().highCompressor();
        }
        return lz4HighCompressor;
    }

    /**
     * Returns a singleton instance of an Lz4 fast compressor.
     * @return A singleton instance of an Lz4 fast compressor.
     */
    public static synchronized LZ4Compressor getLz4FastCompressor() {
        if(lz4FastCompressor == null) {
            lz4FastCompressor = getLz4Factory().fastCompressor();
        }
        return lz4FastCompressor;
    }

    /**
     * Returns a singleton instance of an Lz4 fast decompressor.
     * @return A singleton instance of an Lz4 fast decompressor.
     */
    public static synchronized LZ4FastDecompressor getLz4FastDecompressor() {
        if(lz4FastDecompressor == null) {
            lz4FastDecompressor = getLz4Factory().fastDecompressor();
        }
        return lz4FastDecompressor;
    }

    /**
     * Returns a singleton instance of an Lz4 safe decompressor.
     * @return A singleton instance of an Lz4 safe decompressor.
     */
    public static synchronized LZ4SafeDecompressor getLz4SafeDecompressor() {
        if(lz4SafeDecompressor == null) {
            lz4SafeDecompressor = getLz4Factory().safeDecompressor();
        }
        return lz4SafeDecompressor;
    }

    /**
     * Compress an array of bytes using the high compressor.
     * @param bytes An array of bytes to compress.
     * @return A compressed byte array where the first 4 bytes represent the size of the original uncompressed file.
     */
    public static byte[] compress(final byte[] bytes) {
        return compress(getLz4HighCompressor(), bytes);
    }

    /**
     * Compress an array of bytes using the provided compressor.
     * @param compressor An instance of an Lz4 compressor.
     * @param bytes An array of bytes to compress.
     * @return A compressed byte array where the first 4 bytes represent the size of the original uncompressed file.
     */
    public static byte[] compress(final LZ4Compressor compressor, final byte[] bytes) {
        final byte[] uncompressedSize = encodeUncompressedSize(bytes.length);
        final byte[] compressedData = compressor.compress(bytes);
        final byte[] payload = new byte[uncompressedSize.length + compressedData.length];
        System.arraycopy(uncompressedSize, 0, payload, 0, uncompressedSize.length);
        System.arraycopy(compressedData, 0, payload, uncompressedSize.length, compressedData.length);
        return payload;
    }

    /**
     * Compresses a file on disk and writes the compressed file back to disk.
     * @param from The file to compress.
     * @param to The location of the resulting compressed file where the first 4 bytes represent the size of the
     *           original uncompressed file.
     * @throws IOException If the source file DNE or the result file can not be written.
     */
    public static void compressFile(final Path from, final Path to) throws IOException {
        final byte[] originalBytes = Files.readAllBytes(from);
        final byte[] compressedBytes = compress(originalBytes);
        Files.write(to, compressedBytes);
    }

    /**
     * Decompresses a file on disk using the fast decompressor and writes the decompressed file back to disk.
     * @param from The compressed file to decompress.
     * @param to The location of the resulting decompressed file.
     * @throws IOException If the source file DNE or the result file can not be written.
     */
    public static void decompressFileFast(final Path from, final Path to) throws IOException {
        final byte[] compressedBytes = Files.readAllBytes(from);
        final byte[] decompressed = decompressFast(compressedBytes);
        Files.write(to, decompressed);
    }

    /**
     * Decompresses a file on disk using the safe decompressor and writes the decompressed file back to disk.
     * @param from The compressed file to decompress.
     * @param to The location of the resulting decompressed file.
     * @throws IOException If the source file DNE or the result file can not be written.
     */
    public static void decompressFileSafe(final Path from, final Path to) throws IOException {
        final byte[] compressedBytes = Files.readAllBytes(from);
        final byte[] decompressed = decompressSafe(compressedBytes);
        Files.write(to, decompressed);
    }

    /**
     * Decompresses an array of bytes using the fast decompressor.
     * @param bytes An array of bytes to decompress where the first 4 bytes represent the size of the original
     *              uncompressed byte array.
     * @return An array of decompressed bytes.
     */
    public static byte[] decompressFast(final byte[] bytes) {
        final int uncompressedSize = decodeUncompressedSize(bytes);
        final byte[] compressedData = Arrays.copyOfRange(bytes, 4, bytes.length);
        final byte[] result = new byte[uncompressedSize];
        getLz4FastDecompressor().decompress(compressedData, result);
        return result;
    }

    /**
     * Decompresses an array of bytes using the safe decompressor.
     * @param bytes An array of bytes to decompress where the first 4 bytes represent the size of the original
     *              uncompressed byte array.
     * @return An array of decompressed bytes.
     */
    public static byte[] decompressSafe(final byte[] bytes) {
        final int uncompressedSize = decodeUncompressedSize(bytes);
        final byte[] compressedData = Arrays.copyOfRange(bytes, 4, bytes.length);
        return getLz4SafeDecompressor().decompress(compressedData, uncompressedSize);
    }

    /**
     * Converts 4-bytes that represent the size of the original uncompressed byte array into a single integer.
     * @param bytes The 4 bytes that represent the size of the original uncompressed byte array.
     * @return An integer representing the size of the original uncompressed byte array.
     */
    public static int decodeUncompressedSize(final byte[] bytes) {
        int ret = 0;
        ret |= (int)bytes[0] & 0xFF;
        ret <<= 8;
        ret |= (int)bytes[1] & 0xFF;
        ret <<= 8;
        ret |= (int)bytes[2] & 0xFF;
        ret <<= 8;
        ret |= (int)bytes[3] & 0xFF;
        return ret;
    }

    /**
     * Converts an integer which represents the size of an original uncompressed file in bytes into 4 bytes which are
     * prepended to a compressed LZ4 byte array.
     * @param size The size of the original uncompressed byte array.
     * @return The size of the original uncompressed byte array converted to 4 bytes.
     */
    public static byte[] encodeUncompressedSize(final int size) {
        final byte[] result = new byte[4];
        result[0] = (byte)(size >> 24);
        result[1] = (byte)(size >> 16);
        result[2] = (byte)(size >> 8);
        result[3] = (byte) size;
        return result;
    }
}
