package io.redvox.api900;

import io.redvox.apis.Api900;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.function.Function;

/**
 * This class contains methods for working with RedVox interleaved payloads and providing type generic access methods.
 */
public class PayloadUtil {
    /**
     * Function for extracting values from an interleaved payload and transforming the extracted values.
     * @param list A list of values constituting a sensor payload.
     * @param idx The index to start extracting values from the payload from.
     * @param step The step size for values to extract from the list.
     * @param transform A function from T to R that transforms the extracted payload value.
     * @param <T> The input payload data type.
     * @param <R> The output payload transformed data type.
     * @return A list of transformed and extracted values from an interleaved sensor channel.
     */
    private static <T, R> List<R> extract(final List<T> list, final int idx, final int step, final Function<T, R> transform) {
        final List<R> extracted = new ArrayList<>();
        for (int i = idx; i < list.size(); i += step) {
            extracted.add(transform.apply(list.get(i)));
        }
        return extracted;
    }

    /**
     * Both evenly and unevenly sampled payloads use the same types, this allows us to unify the types between the
     * payload types.
     */
    private enum JointPayloadCase {
        BYTE_PAYLOAD,
        UINT32_PAYLOAD,
        UINT64_PAYLOAD,
        INT32_PAYLOAD,
        INT64_PAYLOAD,
        FLOAT32_PAYLOAD,
        FLOAT64_PAYLOAD,
        PAYLOAD_NOT_SET;
    }

    /**
     * Given an evenly sampled channel, extract the payload type case.
     * @param evenlySampledChannel The channel to extract the payload case from.
     * @return The joint payload case associated with the provided channel.
     */
    private static JointPayloadCase from(final Api900.EvenlySampledChannel evenlySampledChannel) {
        return JointPayloadCase.valueOf(evenlySampledChannel.getPayloadCase().name());
    }

    /**
     * Given an unevenly sampled channel, extract the payload type case.
     * @param unevenlySampledChannel The channel to extract the payload case from.
     * @return The joint payload case associated with the provided channel.
     */
    private static JointPayloadCase from(final Api900.UnevenlySampledChannel unevenlySampledChannel) {
        return JointPayloadCase.valueOf(unevenlySampledChannel.getPayloadCase().name());
    }


    /**
     * Extracts a sensor payload from an interleaved channel from a given index with a given step and transform.
     *
     * Only one of evenly sampled channel or unevenly sampled channel should be passed into the function.
     * The payload case is determined from the passed in channel. The transform function is chosen based off of the
     * type of data stored in the original channel payload.
     *
     * @param evenlySampledChannel An instance of an evenly sampled channel.
     * @param unevenlySampledChannel An instance of an unevenly sampled channel.
     * @param channelIdx The index to start extraction from.
     * @param step The step size to use when extracting data from the payload.
     * @param byteTransform A function to convert byte data to another data type.
     * @param integerTransform A function to convert integer data to another data type.
     * @param longTransform A function to convert long data to another data type.
     * @param floatTransform A function to convert float data to another data type.
     * @param doubleTransform A function to convert double data to another data type.
     * @param <R> The type that the transform functions will return.
     * @return A list of transformed and extracted payload values.
     */
    private static <R> List<R> extractPayload(final Api900.EvenlySampledChannel evenlySampledChannel,
                                              final Api900.UnevenlySampledChannel unevenlySampledChannel,
                                              final int channelIdx,
                                              final int step,
                                              final Function<Byte, R> byteTransform,
                                              final Function<Integer, R> integerTransform,
                                              final Function<Long, R> longTransform,
                                              final Function<Float, R> floatTransform,
                                              final Function<Double, R> doubleTransform) {

        final JointPayloadCase payloadCase = evenlySampledChannel != null ? from(evenlySampledChannel)
                : from(unevenlySampledChannel);

        switch (payloadCase) {
            case BYTE_PAYLOAD:
                final List<R> byteVals = new ArrayList<>();
                final byte[] bytes = evenlySampledChannel != null ? evenlySampledChannel.getBytePayload().getPayload().toByteArray() :
                        unevenlySampledChannel.getBytePayload().getPayload().toByteArray();
                for (int i = channelIdx; i < bytes.length; i += step) {
                    byteVals.add(byteTransform.apply(bytes[i]));
                }
                return byteVals;
            case UINT32_PAYLOAD:
                return PayloadUtil.extract(
                        evenlySampledChannel != null ? evenlySampledChannel.getUint32Payload().getPayloadList()
                                : unevenlySampledChannel.getUint32Payload().getPayloadList(),
                        channelIdx, step, integerTransform);
            case UINT64_PAYLOAD:
                return PayloadUtil.extract(
                        evenlySampledChannel != null ? evenlySampledChannel.getUint64Payload().getPayloadList()
                                : unevenlySampledChannel.getUint64Payload().getPayloadList(),
                        channelIdx, step, longTransform);
            case INT32_PAYLOAD:
                return PayloadUtil.extract(
                        evenlySampledChannel != null ? evenlySampledChannel.getInt32Payload().getPayloadList()
                                : unevenlySampledChannel.getInt32Payload().getPayloadList(),
                        channelIdx, step, integerTransform);
            case INT64_PAYLOAD:
                return PayloadUtil.extract(
                        evenlySampledChannel != null ? evenlySampledChannel.getInt64Payload().getPayloadList()
                                : unevenlySampledChannel.getInt64Payload().getPayloadList(),
                        channelIdx, step, longTransform);
            case FLOAT32_PAYLOAD:
                return PayloadUtil.extract(
                        evenlySampledChannel != null ? evenlySampledChannel.getFloat32Payload().getPayloadList()
                                : unevenlySampledChannel.getFloat32Payload().getPayloadList(),
                        channelIdx, step, floatTransform);
            case FLOAT64_PAYLOAD:
                return PayloadUtil.extract(
                        evenlySampledChannel != null ? evenlySampledChannel.getFloat64Payload().getPayloadList()
                                : unevenlySampledChannel.getFloat64Payload().getPayloadList(),
                        channelIdx, step, doubleTransform);
            case PAYLOAD_NOT_SET:
                return Collections.emptyList();
            default:
                return Collections.emptyList();
        }
    }

    /**
     * Extracts and transforms payload values from evenly or unevenly sampled channels into 64-bit floats.
     *
     * Only one of evenly sampled or unevenly sampled channel should be passed into the function.
     *
     * @param evenlySampledChannel An instance of an evenly sampled channel.
     * @param unevenlySampledChannel An instance of an unevenly sampled channel.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size.
     * @return A list of doubles representing the extracted and transformed payload.
     */
    private static List<Double> extractFloatingPayload(final Api900.EvenlySampledChannel evenlySampledChannel,
                                                       final Api900.UnevenlySampledChannel unevenlySampledChannel,
                                                       final int channelIdx,
                                                       final int step) {
        return extractPayload(evenlySampledChannel,
                unevenlySampledChannel,
                channelIdx,
                step,
                Byte::doubleValue,
                Integer::doubleValue,
                Long::doubleValue,
                Float::doubleValue,
                d -> d);
    }

    /**
     * Extracts and transforms a payload to floating point from a given evenly sampled channel.
     * @param evenlySampledChannel An instance of an evenly sampled channel.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size used for extraction.
     * @return A list of doubles representing the extracted and transformed payload.
     */
    private static List<Double> extractFloatingPayload(final Api900.EvenlySampledChannel evenlySampledChannel,
                                                       final int channelIdx,
                                                       final int step) {
        return extractFloatingPayload(evenlySampledChannel, null, channelIdx, step);
    }

    /**
     * Extracts and transforms a payload to floating point from a given evenly sampled channel.
     * @param evenlySampledChannel An instance of an evenly sampled channel.
     * @return A list of doubles representing the extracted and transformed payload.
     */
    public static List<Double> extractFloatingPayload(final Api900.EvenlySampledChannel evenlySampledChannel) {
        return extractFloatingPayload(evenlySampledChannel, 0, 1);
    }

    /**
     * Extracts and transforms a payload to floating point from a given unevenly sampled channel.
     * @param unevenlySampledChannel An instance of an unevenly sampled channel.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size used for extraction.
     * @return A list of doubles representing the extracted and transformed payload.
     */
    public static List<Double> extractFloatingPayload(final Api900.UnevenlySampledChannel unevenlySampledChannel,
                                                      final int channelIdx,
                                                      final int step) {
        return extractFloatingPayload(null, unevenlySampledChannel, channelIdx, step);
    }

    /**
     * Extracts and transforms a payload to floating point from a given unevenly sampled channel.
     * @param unevenlySampledChannel An instance of an unevenly sampled channel.
     * @return A list of doubles representing the extracted and transformed payload.
     */
    public static List<Double> extractFloatingPayload(final Api900.UnevenlySampledChannel unevenlySampledChannel) {
        return extractFloatingPayload(unevenlySampledChannel, 0, 1);
    }

    /**
     * Extracts and transforms payload values from evenly or unevenly sampled channels into 64-bit integers.
     *
     * Only one of evenly sampled or unevenly sampled channel should be passed into the function.
     *
     * @param evenlySampledChannel An instance of an evenly sampled channel.
     * @param unevenlySampledChannel An instance of an unevenly sampled channel.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size used for extraction.
     * @return A list of longs representing the extracted and transformed payload.
     */
    private static List<Long> extractIntegralPayload(final Api900.EvenlySampledChannel evenlySampledChannel,
                                                     final Api900.UnevenlySampledChannel unevenlySampledChannel,
                                                     final int channelIdx,
                                                     final int step) {
        return extractPayload(evenlySampledChannel,
                unevenlySampledChannel,
                channelIdx,
                step,
                Byte::longValue,
                Integer::longValue,
                l -> l,
                Float::longValue,
                Double::longValue);
    }

    /**
     * Extracts and transforms payload values from evenly or unevenly sampled channels into 64-bit integers.
     * @param evenlySampledChannel An instance of an evenly sampled channel.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size used for extraction.
     * @return A list of longs representing the extracted and transformed payload.
     */
    private static List<Long> extractIntegralPayload(final Api900.EvenlySampledChannel evenlySampledChannel,
                                                     final int channelIdx,
                                                     final int step) {
        return extractIntegralPayload(evenlySampledChannel, null, channelIdx, step);
    }

    /**
     * Extracts and transforms payload values from evenly or unevenly sampled channels into 64-bit integers.
     * @param evenlySampledChannel An instance of an evenly sampled channel.
     * @return A list of longs representing the extracted and transformed payload.
     */
    public static List<Long> extractIntegralPayload(final Api900.EvenlySampledChannel evenlySampledChannel) {
        return extractIntegralPayload(evenlySampledChannel, 0, 1);
    }

    /**
     * Extracts and transforms payload values from evenly or unevenly sampled channels into 64-bit integers.
     * @param unevenlySampledChannel An instance of an unevenly sampled payload.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size used for extraction.
     * @return A list of longs representing the extracted and transformed payload.
     */
    private static List<Long> extractIntegralPayload(final Api900.UnevenlySampledChannel unevenlySampledChannel,
                                                     final int channelIdx,
                                                     final int step) {
        return extractIntegralPayload(null, unevenlySampledChannel, channelIdx, step);
    }

    /**
     * Extracts and transforms payload values from evenly or unevenly sampled channels into 64-bit integers.
     * @param unevenlySampledChannel An instance of an unevenly sampled payload.
     * @return A list of longs representing the extracted and transformed payload.
     */
    public static List<Long> extractIntegralPayload(final Api900.UnevenlySampledChannel unevenlySampledChannel) {
        return extractIntegralPayload(unevenlySampledChannel, 0, 1);
    }

    /**
     * Extracts a byte payload from an array of bytes.
     * @param bytes The bytes to extract the payload from.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size used for extraction.
     * @return A list of bytes representing the byte payload.
     */
    private static List<Byte> extractBytePayload(final byte[] bytes,
                                                 final int channelIdx,
                                                 final int step) {
        final List<Byte> byteList = new ArrayList<>();
        for (int i = channelIdx; i < bytes.length; i++) {
            byteList.add(bytes[i]);
        }
        return byteList;
    }


    /**
     * Extracts a byte payload from an evenly sampled channel.
     * @param evenlySampledChannel An instance of an evenly sampled channel.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size used for extraction.
     * @return A list of bytes representing the byte payload.
     */
    public static List<Byte> extractBytePayload(final Api900.EvenlySampledChannel evenlySampledChannel,
                                                final int channelIdx,
                                                final int step) {
        return extractBytePayload(evenlySampledChannel.getBytePayload().getPayload().toByteArray(),
                channelIdx,
                step);
    }

    /**
     * Extracts a byte payload from an unevenly sampled channel.
     * @param unevenlySampledChannel An instance of an unevenly sampled channel.
     * @param channelIdx The index to begin extraction from.
     * @param step The step size used for extraction.
     * @return A list of bytes representing the byte payload.
     */
    public static List<Byte> extractBytePayload(final Api900.UnevenlySampledChannel unevenlySampledChannel,
                                                final int channelIdx,
                                                final int step) {
        return extractBytePayload(unevenlySampledChannel.getBytePayload().getPayload().toByteArray(),
                channelIdx,
                step);
    }
}
