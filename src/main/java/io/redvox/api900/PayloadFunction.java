package io.redvox.api900;

/**
 * This interface is used for extracting interleaved payloads while transforming the data in the process.
 * @param <T> Data channel type to extract payload from
 * @param <R> Return type.
 */
public interface PayloadFunction<T, R> {
    /**
     * Extract a payload from either an evenly on unevenly sampled channel.
     * @param channel The channel to extract the payload from
     * @param idx The index to start extraction at.
     * @param step The step size.
     * @return Data from the provide channel's payload as type R.
     */
    R apply(T channel, int idx, int step);
}
