package io.redvox.api900;

import com.google.protobuf.InvalidProtocolBufferException;

import com.google.protobuf.util.JsonFormat;
import io.redvox.apis.Api900;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;

/**
 * This class reads RedVox encoded rdvxz files from the file system, byte buffers, and json files.
 * This class also provides methods for wrapping a RedVox encoded file with high-level getters.
 */
public class Reader {
    /**
     * Read RedVox encoded file from file system.
     * @param path Path to RedVox encoded file.
     * @return An optional protobuf representation of a RedVox packet.
     */
    public static Optional<Api900.RedvoxPacket> readFile(final Path path) {
        try {
            return readBuffer(Files.readAllBytes(path));
        } catch(IOException e) {
            System.err.format("Error loading file %s: %s\n", path, e.getMessage());
            return Optional.empty();
        }
    }

    /**
     * Read RedVox encoded file from byte buffer.
     * @param data Byte buffer.
     * @return An optional protobuf representation of a RedVox packet.
     */
    public static Optional<Api900.RedvoxPacket> readBuffer(final byte[] data) {
        try {
            return Optional.of(Api900.RedvoxPacket.parseFrom(Lz4.decompressFast(data)));
        } catch(InvalidProtocolBufferException e) {
            System.err.format("Error loading buffer of size [%d]: %s\n", data.length, e.getMessage());
            return Optional.empty();
        }
    }

    /**
     * Read RedVox encoded file from String representing JSON.
     * @param json Json value of encoded RedVox file.
     * @return An optional protobuf representation of a RedVox packet.
     */
    public static Optional<Api900.RedvoxPacket> readJson(final String json) {
        final Api900.RedvoxPacket.Builder builder = Api900.RedvoxPacket.newBuilder();
        try {
            JsonFormat.parser().merge(json, builder);
            return Optional.of(builder.build());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * Wraps a RedVox encoded protobuf file in a high-level API wrapper.
     * @param redvoxPacket RedVox encoded protobuf representation.
     * @return A WrappedRedvoxPacket with high-level accessors to underlying sensor data.
     */
    public static WrappedRedvoxPacket wrap(final Api900.RedvoxPacket redvoxPacket) {
        return new WrappedRedvoxPacket(redvoxPacket);
    }
}
