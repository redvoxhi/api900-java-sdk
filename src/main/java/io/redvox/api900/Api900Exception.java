package io.redvox.api900;

/**
 * This class defines a custom exception for dealing with runtime errors of RedVox data.
 */
public class Api900Exception extends RuntimeException {
    /**
     * Instantiates a new exception for API 900.
     * @param message The error message.
     */
    public Api900Exception(final String message) {
        super(message);
    }

    /**
     * Convenience method for generating RedVox exceptions.
     * @param message The exception error message.
     * @return An instance of an Api900Exception.
     */
    public static Api900Exception of(final String message) {
        return new Api900Exception(message);
    }
}
