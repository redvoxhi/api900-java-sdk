package io.redvox.api900;

import com.google.protobuf.InvalidProtocolBufferException;
import com.google.protobuf.util.JsonFormat;
import io.redvox.api900.sensors.*;
import io.redvox.apis.Api900;

import java.util.*;
import java.util.function.Function;

/**
 * This class provides high-level accessors for top-level metadata, sensor channel availability, sensor channels,
 * and sensor channel metadata, timestamps, and payloads.
 */
public class WrappedRedvoxPacket {
    /**
     * Cache of evenly sampled channels created on packet load.
     */
    private final Map<Api900.ChannelType, Api900.EvenlySampledChannel> evenlySampledChannelCache;

    /**
     * Cache of unevenly sampled channels created on packet load.
     */
    private final Map<Api900.ChannelType, Api900.UnevenlySampledChannel> unevenlySampledChannelCache;

    /**
     * Reference to original protobuf encoded RedvoxPacket.
     */
    public final Api900.RedvoxPacket redvoxPacket;

    /**
     * Instantiates a new WrappedRedvoxPacket from the Java protobuf representation of an API 900 packet.
     */
    public WrappedRedvoxPacket(final Api900.RedvoxPacket redvoxPacket) {
        this.redvoxPacket = redvoxPacket;
        this.evenlySampledChannelCache = new HashMap<>();
        this.unevenlySampledChannelCache = new HashMap<>();

        for (final Api900.EvenlySampledChannel evenlySampledChannel : redvoxPacket.getEvenlySampledChannelsList()) {
            for (final Api900.ChannelType channelType : evenlySampledChannel.getChannelTypesList()) {
                this.evenlySampledChannelCache.put(channelType, evenlySampledChannel);
            }
        }

        for (final Api900.UnevenlySampledChannel unevenlySampledChannel : redvoxPacket.getUnevenlySampledChannelsList()) {
            for (final Api900.ChannelType channelType : unevenlySampledChannel.getChannelTypesList()) {
                this.unevenlySampledChannelCache.put(channelType, unevenlySampledChannel);
            }
        }
    }

    /**
     * Attempts to return an unevenly sampled channel from the cache and converts the channel into the proper sensor
     * type POJO.
     * @param cache A cache of unevenly sampled channels.
     * @param channelType The channel type that we are attempting to return.
     * @param initializer A function that converts the channel into a POJO of the correct sensor type.
     * @param <T> The type of sensor POJO to covert into.
     * @return An optional of the POJO sensor type.
     */
    private static <T> Optional<T> getUnevenlySampledChannel(final Map<Api900.ChannelType, Api900.UnevenlySampledChannel> cache,
                                                             final Api900.ChannelType channelType,
                                                             final Function<Api900.UnevenlySampledChannel, T> initializer) {
        if (!cache.containsKey(channelType)) {
            return Optional.empty();
        }

        return Optional.of(initializer.apply(cache.get(channelType)));
    }

    /* Top level metadata */

    /**
     * Returns the API level of the RedVox protocol.
     * @return the API level of the RedVox protocol.
     */
    public int api() {
        return this.redvoxPacket.getApi();
    }

    /**
     * Returns the redvox id.
     * @return the redvox id.
     */
    public String redvoxId() {
        return this.redvoxPacket.getRedvoxId();
    }

    /**
     * Returns the RedVox uuid.
     * @return the RedVox uuid.
     */
    public String uuid() {
        return this.redvoxPacket.getUuid();
    }

    /**
     * Returns the authenticated e-mail address used to create this packet.
     * @return the authenticated e-mail address used to create this packet.
     */
    public String authenticatedEmail() {
        return this.redvoxPacket.getAuthenticatedEmail();
    }

    /**
     * Returns the JWT authentication token. May be redacted if this has packet has been received at a RedVox server.
     * @return the JWT authentication token. May be redacted if this has packet has been received at a RedVox server.
     */
    public String authenticationToken() {
        return this.redvoxPacket.getAuthenticationToken();
    }

    /**
     * Returns the firebase token.
     * @return the firebase token.
     */
    public String firebaseToken() {
        return this.redvoxPacket.getFirebaseToken();
    }

    /**
     * Flag that is set for backfilled data.
     * @return True is backfilled, false otherwise.
     */
    public boolean isBackfilled() {
        return this.redvoxPacket.getIsBackfilled();
    }

    /**
     * Flag that is set for private data.
     * @return True if private, false otherwise.
     */
    public boolean isPrivate() {

        return this.redvoxPacket.getIsPrivate();
    }

    /**
     * Flag that is set for if microphone data is scrambled data. Generally only used with 8kHz sample rates.
     * @return True is scrambled, false otherwise.
     */
    public boolean isScrambled() {
        return this.redvoxPacket.getIsScrambled();
    }

    /**
     * Returns the sensor device make.
     * @return the sensor device make.
     */
    public String deviceMake() {
        return this.redvoxPacket.getDeviceMake();
    }

    /**
     * Returns the sensor device model.
     * @return the sensor device model.
     */
    public String deviceModel() {
        return this.redvoxPacket.getDeviceModel();
    }

    /**
     * Returns the operating system of the sensor device.
     * @return the operating system of the sensor device.
     */
    public String deviceOs() {
        return this.redvoxPacket.getDeviceOs();
    }

    /**
     * Returns the operating system version of the sensor device.
     * @return the operating system version of the sensor device.
     */
    public String deviceOsVersion() {
        return this.redvoxPacket.getDeviceOsVersion();
    }

    /**
     * Returns the app version of the sensor device.
     * @return the app version of the sensor device.
     */
    public String appVersion() {
        return this.redvoxPacket.getAppVersion();
    }

    /**
     * Returns the battery level of the sensor device as a percentage.
     * @return the battery level of the sensor device as a percentage.
     */
    public float batteryLevelPercent() {
        return this.redvoxPacket.getBatteryLevelPercent();
    }

    /**
     * Returns the temperature of the sensor device in degrees Celsius.
     * @return the temperature of the sensor device in degrees Celsius.
     */
    public float deviceTemperatureC() {
        return this.redvoxPacket.getDeviceTemperatureC();
    }

    /**
     * Returns the URL of the server this packet was acquired at.
     * @return the URL of the server this packet was acquired at.
     */
    public String acquisitionServer() {
        return this.redvoxPacket.getAcquisitionServer();
    }

    /**
     * Returns the URL of the server where time synchronization exchanges took place.
     * @return the URL of the server where time synchronization exchanges took place.
     */
    public String timeSynchronizationServer() {
        return this.redvoxPacket.getTimeSynchronizationServer();
    }

    /**
     * Returns the URL of the server that the sensor device authenticated itself with.
     * @return the URL of the server that the sensor device authenticated itself with.
     */
    public String authenticationServer() {
        return this.redvoxPacket.getAuthenticationServer();
    }

    /**
     * Returns the number of microseconds since the epoch UTC marking the creation of this packet as calculated by
     * the sensor device's clock.
     * Generally corresponds to the first sample of the audio channel.
     * @return the number of microseconds since the epoch UTC marking the creation of this packet from sensor's clock.
     */
    public long appFileStartTimestampEpochMicrosecondsUtc() {
        return this.redvoxPacket.getAppFileStartTimestampEpochMicrosecondsUtc();
    }

    /**
     * Returns the number of microseconds since the epoch UTC which marks the creation of this packet as calculated by
     * the sensor device's machine time.
     * Generally corresponds to the first sample of the audio channel.
     * @return the number of microseconds since the epoch UTC marking the creation of this packet from sensor's machine
     * time.
     */
    public long appFileStartTimestampMachine() {
        return this.redvoxPacket.getAppFileStartTimestampMachine();
    }

    /**
     * Returns the number of microseconds since the epoch UTC that marks this packet's arrival at the acquisition
     * server.
     * @return the number of microseconds since the epoch UTC that marks this packet's arrival at the acquisition
     * server.
     */
    public long serverTimestampEpochMicrosecondsUtc() {
        return this.redvoxPacket.getServerTimestampEpochMicrosecondsUtc();
    }

    /**
     * Returns any top-level metadata added to this packet as a list.
     * @return any top-level metadata added to this packet as a list.
     */
    public List<String> metadataList() {
        return this.redvoxPacket.getMetadataList();
    }

    /**
     * Returns any top-level metadata added to this packet as key-values.
     * @return any top-level metadata added to this packet as key-values.
     */
    public Map<String, String> metadataMap() {
        return Util.listToKeyPair(this.metadataList());
    }

    /* Sensor/channel access */

    /**
     * Returns if this packet contains a microphone sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasMicrophoneChannel() {
        return this.evenlySampledChannelCache.containsKey(Api900.ChannelType.MICROPHONE);
    }

    /**
     * Returns the microphone channel in an optional if it is present, otherwise, an empty optional.
     * @return the microphone channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<MicrophoneSensor> microphoneChannel() {
        if (!hasMicrophoneChannel()) {
            return Optional.empty();
        }
        return Optional.of(new MicrophoneSensor(this.evenlySampledChannelCache.get(Api900.ChannelType.MICROPHONE)));
    }

    /**
     * Returns if this packet contains a barometer sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasBarometerChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.BAROMETER);
    }

    /**
     * Returns the barometer channel in an optional if it is present, otherwise, an empty optional.
     * @return the barometer channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<BarometerSensor> barometerChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.BAROMETER,
                BarometerSensor::new);
    }

    /**
     * Returns if this packet contains a location sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasLocationChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.LATITUDE);
    }

    /**
     * Returns the location channel in an optional if it is present, otherwise, an empty optional.
     * @return the location channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<LocationSensor> locationChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.LATITUDE,
                LocationSensor::new);
    }

    /**
     * Returns if this packet contains a time synchronization sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasTimeSynchronizationChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.TIME_SYNCHRONIZATION);
    }

    /**
     * Returns the time synchronization channel in an optional if it is present, otherwise, an empty optional.
     * @return the time synchronization channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<TimeSynchronizationSensor> timeSynchronizationChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.TIME_SYNCHRONIZATION,
                TimeSynchronizationSensor::new);
    }

    /**
     * Returns if this packet contains an accelerometer sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasAccelerometerChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.ACCELEROMETER_X);
    }

    /**
     * Returns the accelerometer channel in an optional if it is present, otherwise, an empty optional.
     * @return the accelerometer channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<AccelerometerSensor> accelerometerChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.ACCELEROMETER_X,
                AccelerometerSensor::new);
    }

    /**
     * Returns if this packet contains a magnetometer sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasMagnetometerChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.MAGNETOMETER_X);
    }

    /**
     * Returns the magnetometer channel in an optional if it is present, otherwise, an empty optional.
     * @return the magnetometer channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<MagnetometerSensor> magnetometerChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.MAGNETOMETER_X,
                MagnetometerSensor::new);
    }

    /**
     * Returns if this packet contains a gyroscope sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasGyroscopeChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.GYROSCOPE_X);
    }

    /**
     * Returns the gyroscope channel in an optional if it is present, otherwise, an empty optional.
     * @return the gyroscope channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<GyroscopeSensor> gyroscopeChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.GYROSCOPE_X,
                GyroscopeSensor::new);
    }

    /**
     * Returns if this packet contains a light sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasLightChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.LIGHT);
    }

    /**
     * Returns the light channel in an optional if it is present, otherwise, an empty optional.
     * @return the light channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<LightSensor> lightChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.LIGHT,
                LightSensor::new);
    }

    /**
     * Returns if this packet contains an infrared sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasInfraredChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.INFRARED);
    }

    /**
     * Returns the infrared channel in an optional if it is present, otherwise, an empty optional.
     * @return the infrared channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<InfraredSensor> infraredChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.INFRARED,
                InfraredSensor::new);
    }

    /**
     * Returns if this packet contains an image sensor channel.
     * @return True if it does, false otherwise.
     */
    public boolean hasImageChannel() {
        return this.unevenlySampledChannelCache.containsKey(Api900.ChannelType.IMAGE);
    }

    /**
     * Returns the image channel in an optional if it is present, otherwise, an empty optional.
     * @return the image channel in an optional if it is present, otherwise, an empty optional.
     */
    public Optional<ImageSensor> imageChannel() {
        return getUnevenlySampledChannel(this.unevenlySampledChannelCache,
                Api900.ChannelType.IMAGE,
                ImageSensor::new);
    }

    /**
     * Attempts to convert the underlying encoded protobuf message to JSON.
     * @return An optional either containing the JSON on successful conversion or an empty optional.
     */
    public Optional<String> toJson() {
        try {
            return Optional.of(JsonFormat.printer().print(redvoxPacket));
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
            return Optional.empty();
        }
    }

    /**
     * Use protobuf-java's String representation.
     * @return Protobuf's String representation of the packet.
     */
    @Override
    public String toString() {
        return this.redvoxPacket.toString();
    }
}
