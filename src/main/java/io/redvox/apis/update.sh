#!/usr/bin/env bash

wget https://bitbucket.org/redvoxhi/redvox-data-apis/raw/master/src/api900/api900.proto -O api900.proto
wget https://bitbucket.org/redvoxhi/redvox-data-apis/raw/master/src/api900/generated_code/java/io/redvox/apis/Api900.java -O Api900.java