package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.WrappedRedvoxPacket;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.Assert.*;


public class WrappedRedvoxPacketTest {
    private WrappedRedvoxPacket wrappedExampleRedvoxPacket;
    private WrappedRedvoxPacket syntheticPacket;

    @Before
    public void setUp() throws Exception {
        this.wrappedExampleRedvoxPacket = Reader.wrap(Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314)).get());
        final String json = new String(Files.readAllBytes(Resources.loadResourcePath(Resources.MOCK_0)));
        this.syntheticPacket = Reader.wrap(Reader.readJson(json).get());
    }

    @Test
    public void notNullPacketRef() {
        assertNotNull(this.wrappedExampleRedvoxPacket.redvoxPacket);
        assertNotNull(this.syntheticPacket.redvoxPacket);
    }

    @Test
    public void api() {
        assertEquals(this.wrappedExampleRedvoxPacket.api(), 900);
        assertEquals(this.syntheticPacket.api(), 900);
    }

    @Test
    public void redvoxId() {
        assertEquals(this.wrappedExampleRedvoxPacket.redvoxId(),"0000001314");
        assertEquals(this.syntheticPacket.redvoxId(),"0000000001");
    }

    @Test
    public void uuid() {
        assertEquals(this.wrappedExampleRedvoxPacket.uuid(),"1711635427");
        assertEquals(this.syntheticPacket.uuid(),"2");
    }

    @Test
    public void authenticatedEmail() {
        assertEquals(this.wrappedExampleRedvoxPacket.authenticatedEmail(),"redvoxcore@gmail.com");
        assertEquals(this.syntheticPacket.authenticatedEmail(),"foo@bar.com");
    }

    @Test
    public void authenticationToken() {
        assertEquals(this.wrappedExampleRedvoxPacket.authenticationToken(),"redacted-953706190");
        assertEquals(this.syntheticPacket.authenticationToken(),"redacted-953706190");
    }

    @Test
    public void firebaseToken() {
        assertEquals(this.wrappedExampleRedvoxPacket.firebaseToken(),"eH7AorhaHeI:APA91bF1X732uhdsAWU494wY8o5CL7clXHSi56AxZPIfTBBIQCmV78HJKerzXQYVMpPuGov-zZ0YPUaTOcoiHCEhJcQQQU1a4mDLBNUgagcwcGEo1D7a89tz66EdcKndSAXY09UgmZVw");
        assertEquals(this.syntheticPacket.firebaseToken(),"foobar");
    }

    @Test
    public void isBackfiled() {
        assertFalse(this.wrappedExampleRedvoxPacket.isBackfilled());
        assertFalse(this.syntheticPacket.isBackfilled());
    }

    @Test
    public void isPrivate() {
        assertTrue(this.wrappedExampleRedvoxPacket.isPrivate());
        assertTrue(this.syntheticPacket.isPrivate());
    }

    @Test
    public void isScrambled() {
        assertFalse(this.wrappedExampleRedvoxPacket.isScrambled());
        assertFalse(this.syntheticPacket.isScrambled());
    }

    @Test
    public void deviceMake() {
        assertEquals(this.wrappedExampleRedvoxPacket.deviceMake(), "Google");
        assertEquals(this.syntheticPacket.deviceMake(), "Google");
    }

    @Test
    public void deviceModel() {
        assertEquals(this.wrappedExampleRedvoxPacket.deviceModel(), "Pixel XL");
        assertEquals(this.syntheticPacket.deviceModel(), "Pixel XL");
    }

    @Test
    public void deviceOs() {
        assertEquals(this.wrappedExampleRedvoxPacket.deviceOs(), "Android");
        assertEquals(this.syntheticPacket.deviceOs(), "Android");
    }

    @Test
    public void deviceOsVersion() {
        assertEquals(this.wrappedExampleRedvoxPacket.deviceOsVersion(), "9");
        assertEquals(this.syntheticPacket.deviceOsVersion(), "9");
    }

    @Test
    public void appVersion() {
        assertEquals(this.wrappedExampleRedvoxPacket.appVersion(), "2.4.P1");
        assertEquals(this.syntheticPacket.appVersion(), "2.4.P1");
    }

    @Test
    public void batteryLevelPercent() {
        assertEquals(this.wrappedExampleRedvoxPacket.batteryLevelPercent(), 89.0, .01);
        assertEquals(this.syntheticPacket.batteryLevelPercent(), 89.0, .01);
    }

    @Test
    public void deviceTemperatureC() {
        assertEquals(this.wrappedExampleRedvoxPacket.deviceTemperatureC(), 32.2, 0.1);
        assertEquals(this.syntheticPacket.deviceTemperatureC(), 32.2, 0.1);
    }

    @Test
    public void acquisitionServer() {
        assertEquals(this.wrappedExampleRedvoxPacket.acquisitionServer(), "NA");
        assertEquals(this.syntheticPacket.acquisitionServer(), "wss://foo:8000");
    }

    @Test
    public void timeSynchronizationServer() {
        assertEquals(this.wrappedExampleRedvoxPacket.timeSynchronizationServer(), "wss://redvox.io/synch/v2");
        assertEquals(this.syntheticPacket.timeSynchronizationServer(), "wss://redvox.io/synch/v2");
    }

    @Test
    public void authenticationServer() {
        assertEquals(this.wrappedExampleRedvoxPacket.authenticationServer(), "https://redvox.io/login/mobile");
        assertEquals(this.syntheticPacket.authenticationServer(), "https://redvox.io/login/mobile");
    }

    @Test
    public void appFileStartTimestampEpochMicrosecondsUtc() {
        assertEquals(this.wrappedExampleRedvoxPacket.appFileStartTimestampEpochMicrosecondsUtc(), 1539627249224471L);
        assertEquals(this.syntheticPacket.appFileStartTimestampEpochMicrosecondsUtc(), 1539627249224471L);
    }

    @Test
    public void appFileStartTimestampMachine() {
        assertEquals(this.wrappedExampleRedvoxPacket.appFileStartTimestampMachine(), 1539627249223992L);
        assertEquals(this.syntheticPacket.appFileStartTimestampMachine(), 1539627249223992L);
    }

    @Test
    public void serverTimestampEpochMicrosecondsUtc() {
        assertEquals(this.wrappedExampleRedvoxPacket.serverTimestampEpochMicrosecondsUtc(), 1539627309435000L);
        assertEquals(this.syntheticPacket.serverTimestampEpochMicrosecondsUtc(), 1539627309435000L);
    }

    @Test
    public void metadataList() {
        assertEquals(this.wrappedExampleRedvoxPacket.metadataList().size(), 0);
        assertEquals(this.syntheticPacket.metadataList(), Arrays.asList("a", "b"));
    }

    @Test
    public void metadataMap() {
        assertEquals(this.wrappedExampleRedvoxPacket.metadataMap().size(), 0);
        assertEquals(this.syntheticPacket.metadataMap().get("a"), "b");
    }

    @Test
    public void hasMicrophoneChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasMicrophoneChannel());
        assertTrue(this.syntheticPacket.hasMicrophoneChannel());
    }

    @Test
    public void microphoneChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.microphoneChannel().isPresent());
        assertTrue(this.syntheticPacket.microphoneChannel().isPresent());
    }

    @Test
    public void hasBarometerChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasBarometerChannel());
        assertTrue(this.syntheticPacket.hasBarometerChannel());
    }

    @Test
    public void barometerChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.barometerChannel().isPresent());
        assertTrue(this.syntheticPacket.barometerChannel().isPresent());
    }

    @Test
    public void hasLocationChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasLocationChannel());
        assertTrue(this.syntheticPacket.hasLocationChannel());
    }

    @Test
    public void locationChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.locationChannel().isPresent());
        assertTrue(this.syntheticPacket.locationChannel().isPresent());
    }

    @Test
    public void hasTimeSynchronizationChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasTimeSynchronizationChannel());
        assertTrue(this.syntheticPacket.hasTimeSynchronizationChannel());
    }

    @Test
    public void timeSynchronizationChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.timeSynchronizationChannel().isPresent());
        assertTrue(this.syntheticPacket.timeSynchronizationChannel().isPresent());
    }

    @Test
    public void hasAccelerometerChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasAccelerometerChannel());
        assertTrue(this.syntheticPacket
                .hasAccelerometerChannel());
    }

    @Test
    public void accelerometerChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.accelerometerChannel().isPresent());
        assertTrue(this.syntheticPacket.accelerometerChannel().isPresent());
    }

    @Test
    public void hasMagnetometerChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasMagnetometerChannel());
        assertTrue(this.syntheticPacket.hasMagnetometerChannel());
    }

    @Test
    public void magnetometerChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.magnetometerChannel().isPresent());
        assertTrue(this.syntheticPacket.magnetometerChannel().isPresent());
    }

    @Test
    public void hasGyroscopeChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasGyroscopeChannel());
        assertTrue(this.syntheticPacket.hasGyroscopeChannel());
    }

    @Test
    public void gyroscopeChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.gyroscopeChannel().isPresent());
        assertTrue(this.syntheticPacket.gyroscopeChannel().isPresent());
    }

    @Test
    public void hasLightChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasLightChannel());
        assertTrue(this.syntheticPacket.hasLightChannel());
    }

    @Test
    public void lightChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.lightChannel().isPresent());
        assertTrue(this.syntheticPacket.lightChannel().isPresent());
    }

    @Test
    public void hasInfraredChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasInfraredChannel());
        assertTrue(this.syntheticPacket.hasInfraredChannel());
    }

    @Test
    public void infraredChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.infraredChannel().isPresent());
        assertTrue(this.syntheticPacket.infraredChannel().isPresent());
    }

    @Test
    public void hasImageChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.hasImageChannel());
        assertFalse(this.syntheticPacket.hasImageChannel());
    }

    @Test
    public void imageChannel() {
        assertTrue(this.wrappedExampleRedvoxPacket.imageChannel().isPresent());
        assertFalse(this.syntheticPacket.imageChannel().isPresent());
    }
}