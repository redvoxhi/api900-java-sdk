package io.redvox.api900.tests;

import io.redvox.api900.Lz4;
import net.jpountz.lz4.LZ4Factory;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class Lz4Test {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getLz4Factory() {
        assertNotNull(Lz4.getLz4Factory());
    }

    @Test
    public void getLz4HighCompressor() {
        assertNotNull(Lz4.getLz4HighCompressor());
    }

    @Test
    public void getLz4FastCompressor() {
        assertNotNull(Lz4.getLz4FastCompressor());
    }

    @Test
    public void getLz4FastDecompressor() {
        assertNotNull(Lz4.getLz4FastDecompressor());
    }

    @Test
    public void getLz4SafeDecompressor() {
        assertNotNull(Lz4.getLz4SafeDecompressor());
    }

    @Test
    public void compress() {
    }

    @Test
    public void compress1() {
    }

    @Test
    public void compressFile() {
    }

    @Test
    public void decompressFileFast() {
    }

    @Test
    public void decompressFileSafe() {
    }

    @Test
    public void decompressFast() {
    }

    @Test
    public void decompressFast1() {
    }

    @Test
    public void decompressFast2() {
    }

    @Test
    public void decompressSafe() {
    }

    @Test
    public void decompressSafe1() {
    }

    @Test
    public void decompressSafe2() {
    }

    @Test
    public void decodeUncompressedSize() {
    }

    @Test
    public void encodeUncompressedSize() {
    }
}