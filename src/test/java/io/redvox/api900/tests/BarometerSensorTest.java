package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.sensors.BarometerSensor;
import io.redvox.api900.sensors.MagnetometerSensor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BarometerSensorTest {
    private BarometerSensor exampleSensor;
    private BarometerSensor syntheticSensor;

    @Before
    public void setUp() throws Exception {
        this.exampleSensor = Reader.wrap(
                Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314))
                        .get())
                .barometerChannel()
                .get();
        this.syntheticSensor = Reader.wrap(
                Reader.readJson(Resources.loadResourceJson(Resources.MOCK_0))
                        .get())
                .barometerChannel()
                .get();
    }

    @Test
    public void testIsCorrectSensor() {
        assertEquals("BMP285 pressure", this.exampleSensor.sensorName());
        assertEquals("BMP285 pressure", this.syntheticSensor.sensorName());
    }
}