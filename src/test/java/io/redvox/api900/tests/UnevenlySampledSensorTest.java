package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.WrappedRedvoxPacket;
import io.redvox.api900.sensors.EvenlySampledSensor;
import io.redvox.api900.sensors.UnevenlySampledSensor;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.util.List;

import static org.junit.Assert.*;

public class UnevenlySampledSensorTest {
    private WrappedRedvoxPacket exampleRedvoxPacket;
    private WrappedRedvoxPacket syntheticPacket;
    private UnevenlySampledSensor exampleUnevenlySampledSensor;
    private UnevenlySampledSensor syntheticUnevenlySampledSensor;

    @Before
    public void setUp() throws Exception {
        this.exampleRedvoxPacket = Reader.wrap(Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314)).get());
        final String json = new String(Files.readAllBytes(Resources.loadResourcePath(Resources.MOCK_0)));
        this.syntheticPacket = Reader.wrap(Reader.readJson(json).get());
        this.exampleUnevenlySampledSensor = this.exampleRedvoxPacket.barometerChannel().get();
        this.syntheticUnevenlySampledSensor = this.syntheticPacket.barometerChannel().get();
    }

//    @Test
//    public void payloadFromChannel() {
//        List<Double> examplePayload = this.exampleUnevenlySampledSensor.paylo
//    }
//
//    @Test
//    public void meanFromChannel() {
//    }
//
//    @Test
//    public void medianFromChannel() {
//    }
//
//    @Test
//    public void stdFromChannel() {
//    }

    @Test
    public void sensorName() {
        assertEquals("BMP285 pressure", this.exampleUnevenlySampledSensor.sensorName());
        assertEquals("BMP285 pressure", this.syntheticUnevenlySampledSensor.sensorName());
    }

    @Test
    public void payloadType() {
        assertEquals("FLOAT32_PAYLOAD", this.exampleUnevenlySampledSensor.payloadType());
        assertEquals("FLOAT32_PAYLOAD", this.syntheticUnevenlySampledSensor.payloadType());
    }

    @Test
    public void timestampsMicrosecondsUtc() {
        assertEquals(251, this.exampleUnevenlySampledSensor.timestampsMicrosecondsUtc().size());
        assertTrue(Util.listsEqual(this.syntheticUnevenlySampledSensor.timestampsMicrosecondsUtc(), 1L, 2L, 3L));
    }

    @Test
    public void sampleIntervalMean() {
        assertEquals(198730.316, this.exampleUnevenlySampledSensor.sampleIntervalMean(), 0.0);
        assertEquals(1.0, this.syntheticUnevenlySampledSensor.sampleIntervalMean(), 0.0);
    }

    @Test
    public void sampleIntervalMedian() {
        assertEquals(198696.5, this.exampleUnevenlySampledSensor.sampleIntervalMedian(), 0.0);
        assertEquals(3.0, this.syntheticUnevenlySampledSensor.sampleIntervalMedian(), 0.0);
    }

    @Test
    public void sampleIntervalStd() {
        assertEquals(22868.158708565585, this.exampleUnevenlySampledSensor.sampleIntervalStd(), 0.0);
        assertEquals(2.0, this.syntheticUnevenlySampledSensor.sampleIntervalStd(), 0.0);
    }

    @Test
    public void metadataList() {
        assertEquals(0, this.exampleUnevenlySampledSensor.metadataList().size());
        assertTrue(Util.listsEqual(this.syntheticUnevenlySampledSensor.metadataList(), "a", "b"));
    }

    @Test
    public void metadataMap() {
        assertEquals(0, this.exampleUnevenlySampledSensor.metadataMap().size());
        assertEquals("b", this.syntheticUnevenlySampledSensor.metadataMap().get("a"));
    }

    @Test
    public void payloadValues() {
        assertEquals(251, this.exampleUnevenlySampledSensor.payloadValues().size());
        assertTrue(Util.listsEqual(this.syntheticUnevenlySampledSensor.payloadValues(), 1.0, 2.0, 3.0));
    }

    @Test
    public void payloadMean() {
        assertEquals(99.77476765909992, this.exampleUnevenlySampledSensor.payloadMean(), 0.0);
        assertEquals(4.0, this.syntheticUnevenlySampledSensor.payloadMean(), 0.0);
    }

    @Test
    public void payloadMedian() {
        assertEquals(99.77483367919922, this.exampleUnevenlySampledSensor.payloadMedian(), 0.0);
        assertEquals(6.0, this.syntheticUnevenlySampledSensor.payloadMedian(), 0.0);
    }

    @Test
    public void payloadStd() {
        assertEquals(0.0016480541128755422, this.exampleUnevenlySampledSensor.payloadStd(), 0.0);
        assertEquals(5.0, this.syntheticUnevenlySampledSensor.payloadStd(), 0.0);
    }
}