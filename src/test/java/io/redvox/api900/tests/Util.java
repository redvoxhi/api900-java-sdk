package io.redvox.api900.tests;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Util {
    public static <T> boolean listsEqual(final List<T> l1, final List<T> l2) {
        if(l1.size() != l2.size()) {
            return false;
        }

        final Iterator<T> l1It = l1.iterator();
        final Iterator<T> l2It = l2.iterator();

        while(l1It.hasNext() && l2It.hasNext()) {
            if(!l1It.next().equals(l2It.next())) {
                return false;
            }
        }

        return true;
    }

    public static <T> boolean listsEqual(final List<T> l1, T... vs) {
        return listsEqual(l1, Arrays.asList(vs));
    }
}
