package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.sensors.GyroscopeSensor;
import io.redvox.api900.sensors.MagnetometerSensor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class MagnetometerSensorTest {
    private MagnetometerSensor exampleSensor;
    private MagnetometerSensor syntheticSensor;

    @Before
    public void setUp() throws Exception {
        this.exampleSensor = Reader.wrap(
                Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314))
                        .get())
                .magnetometerChannel()
                .get();
        this.syntheticSensor = Reader.wrap(
                Reader.readJson(Resources.loadResourceJson(Resources.MOCK_0))
                        .get())
                .magnetometerChannel()
                .get();
    }

    @Test
    public void testIsCorrectSensor() {
        assertEquals("AK09915 magnetometer", this.exampleSensor.sensorName());
        assertEquals("AK09915 magnetometer", this.syntheticSensor.sensorName());
    }

    @Test
    public void testPayloadParts() {
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesX(), 1.0, 4.0, 7.0));
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesY(), 2.0, 5.0, 8.0));
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesZ(), 3.0, 6.0, 9.0));
    }
}