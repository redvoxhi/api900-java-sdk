package io.redvox.api900.tests.mock;

import io.redvox.api900.Reader;
import io.redvox.api900.Writer;
import io.redvox.api900.tests.Resources;
import io.redvox.apis.Api900;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class MockRedvoxPacket {
    public static Api900.RedvoxPacket fromFile(final Path path) {
        try {
            final List<String> lines = Files.readAllLines(path);

            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args) throws IOException {
        final String json = Writer.writeJson(Reader.wrap(Reader.readFile(Resources.loadResourcePath(Resources.PACKET_1314))
                .get()).redvoxPacket);
        System.out.println(json);
        Files.write(Paths.get("mock_0.json"), json.getBytes());
    }
}
