package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.sensors.InfraredSensor;
import io.redvox.api900.sensors.MicrophoneSensor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MicrophoneSensorTest {
    private MicrophoneSensor exampleSensor;
    private MicrophoneSensor syntheticSensor;

    @Before
    public void setUp() throws Exception {
        this.exampleSensor = Reader.wrap(
                Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314))
                        .get())
                .microphoneChannel()
                .get();
        this.syntheticSensor = Reader.wrap(
                Reader.readJson(Resources.loadResourceJson(Resources.MOCK_0))
                        .get())
                .microphoneChannel()
                .get();
    }

    @Test
    public void testIsCorrectSensor() {
        assertEquals("I/INTERNAL MIC", this.exampleSensor.sensorName());
        assertEquals("test sensor name", this.syntheticSensor.sensorName());
    }
}