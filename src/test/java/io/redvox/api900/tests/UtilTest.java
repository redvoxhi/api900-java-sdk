package io.redvox.api900.tests;

import io.redvox.api900.Api900Exception;
import io.redvox.api900.Reader;
import io.redvox.api900.Util;
import io.redvox.api900.Writer;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

import static org.junit.Assert.*;

public class UtilTest {

    @Test
    public void listToKeyPairEmpty() {
        final List<String> emptyList = Collections.emptyList();
        final Map<String, String> emptyMap = Collections.emptyMap();

        assertEquals(emptyMap, Util.listToKeyPair(emptyList));
    }

    @Test(expected = Api900Exception.class)
    public void listToKeyPairSingle() {
        Util.listToKeyPair(Collections.singletonList("foo"));
    }

    @Test(expected = Api900Exception.class)
    public void listToKeyPairOdd() {
        Util.listToKeyPair(Arrays.asList("1", "2", "3"));
    }

    @Test
    public void testTwo() {
        final Map<String, String> map = new HashMap<>();
        map.put("1", "2");
        assertEquals(map, Util.listToKeyPair(Arrays.asList("1", "2")));
    }

    @Test
    public void testEven() {
        final Map<String, String> map = new HashMap<>();
        map.put("1", "2");
        map.put("3", "4");
        assertEquals(map, Util.listToKeyPair(Arrays.asList("1", "2", "3", "4")));
    }

    @Test(expected = Api900Exception.class)
    public void testMeanEmpty() {
        Util.mean(Collections.emptyList());
    }

    @Test
    public void testMeanSingle() {
        assertEquals(3.0, Util.mean(Collections.singletonList(3.0)), 0.0);
    }

    @Test
    public void testMeanDouble() {
        assertEquals(5.0, Util.mean(Arrays.asList(7.0, 3.0)), 0.0);
    }

    @Test
    public void testMeanMulti() {
        assertEquals(3.0, Util.mean(Arrays.asList(1, 2, 3, 4, 5)), 0.0);
    }

    @Test
    public void testMeanNegative() {
        assertEquals(-3.0, Util.mean(Arrays.asList(-1, -2, -3, -4, -5)), 0.0);
    }

    @Test(expected = Api900Exception.class)
    public void testMedianEmpty() {
        Util.<Integer>median(Collections.emptyList());
    }

    @Test
    public void testMedianSingle() {
        assertEquals(3.0, Util.median(Collections.singletonList(3.0)), 0.0);
    }

    @Test
    public void testMedianTwo() {
        assertEquals(5.0, Util.median(Arrays.asList(4, 6)), 0.0);
    }

    @Test
    public void testMedianThree() {
        assertEquals(5.0, Util.median(Arrays.asList(4, 5, 6)), 0.0);
    }

    @Test(expected = Api900Exception.class)
    public void testVarianceEmptyOrOne() {
        Util.variance(Collections.<Integer>emptyList());
        Util.variance(Collections.singletonList(1));
    }

    @Test
    public void testVarianceTwo() {
        assertEquals(0.25, Util.variance(Arrays.asList(1, 2)), 0.0001);
    }

    @Test
    public void testVarianceMulti() {
        assertEquals(2.0, Util.variance(Arrays.asList(1, 2, 3, 4, 5)), 0.0001);
    }

    @Test(expected = Api900Exception.class)
    public void testStdDevEmptyOrONe() {
        Util.stddev(Collections.<Integer>emptyList());
        Util.stddev(Collections.singletonList(1));
    }

    @Test
    public void testStdDevMulti() {
        assertEquals(1.4142135623730951, Util.stddev(Arrays.asList(1, 2, 3, 4, 5)), 0.0001);
    }
}