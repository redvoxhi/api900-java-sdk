package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.sensors.AccelerometerSensor;
import io.redvox.api900.sensors.GyroscopeSensor;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class GyroscopeSensorTest {
    private GyroscopeSensor exampleSensor;
    private GyroscopeSensor syntheticSensor;

    @Before
    public void setUp() throws Exception {
        this.exampleSensor = Reader.wrap(
                Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314))
                        .get())
                .gyroscopeChannel()
                .get();
        this.syntheticSensor = Reader.wrap(
                Reader.readJson(Resources.loadResourceJson(Resources.MOCK_0))
                        .get())
                .gyroscopeChannel()
                .get();
    }

    @Test
    public void testIsCorrectSensor() {
        assertEquals("BMI160 gyroscope", this.exampleSensor.sensorName());
        assertEquals("BMI160 gyroscope", this.syntheticSensor.sensorName());
    }

    @Test
    public void testPayloadParts() {
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesX(), 1.0, 4.0, 7.0));
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesY(), 2.0, 5.0, 8.0));
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesZ(), 3.0, 6.0, 9.0));
    }
}