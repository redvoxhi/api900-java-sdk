package io.redvox.api900.tests;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class PayloadUtilTest {

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void extractPayload() {
    }

    @Test
    public void extractFloatingPayload() {
    }

    @Test
    public void extractFloatingPayload1() {
    }

    @Test
    public void extractFloatingPayload2() {
    }

    @Test
    public void extractFloatingPayload3() {
    }

    @Test
    public void extractFloatingPayload4() {
    }

    @Test
    public void extractIntegralPayload() {
    }

    @Test
    public void extractIntegralPayload1() {
    }

    @Test
    public void extractIntegralPayload2() {
    }

    @Test
    public void extractIntegralPayload3() {
    }

    @Test
    public void extractIntegralPayload4() {
    }

    @Test
    public void extractBytePayload() {
    }

    @Test
    public void extractBytePayload1() {
    }
}