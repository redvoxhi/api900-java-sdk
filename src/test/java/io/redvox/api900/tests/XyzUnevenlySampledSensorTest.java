package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.WrappedRedvoxPacket;
import io.redvox.api900.sensors.XyzUnevenlySampledSensor;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;

import static org.junit.Assert.*;

public class XyzUnevenlySampledSensorTest {
    private WrappedRedvoxPacket exampleRedvoxPacket;
    private WrappedRedvoxPacket syntheticPacket;
    private XyzUnevenlySampledSensor exampleXyzSensor;
    private XyzUnevenlySampledSensor syntheticXyzSensor;

    @Before
    public void setUp() throws Exception {
        this.exampleRedvoxPacket = Reader.wrap(Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314)).get());
        final String json = new String(Files.readAllBytes(Resources.loadResourcePath(Resources.MOCK_0)));
        this.syntheticPacket = Reader.wrap(Reader.readJson(json).get());
        this.exampleXyzSensor = this.exampleRedvoxPacket.magnetometerChannel().get();
        this.syntheticXyzSensor = this.syntheticPacket.magnetometerChannel().get();
    }

    @Test
    public void payloadValuesX() {
        final int third = this.exampleXyzSensor.payloadValues().size() / 3;
        assertEquals(third, this.exampleXyzSensor.payloadValuesX().size());
        assertTrue(Util.listsEqual(this.syntheticXyzSensor.payloadValuesX(), 1.0, 4.0, 7.0));
    }

    @Test
    public void payloadValuesY() {
        final int third = this.exampleXyzSensor.payloadValues().size() / 3;
        assertEquals(third, this.exampleXyzSensor.payloadValuesY().size());
        assertTrue(Util.listsEqual(this.syntheticXyzSensor.payloadValuesY(), 2.0, 5.0, 8.0));
    }

    @Test
    public void payloadValuesZ() {
        final int third = this.exampleXyzSensor.payloadValues().size() / 3;
        assertEquals(third, this.exampleXyzSensor.payloadValuesZ().size());
        assertTrue(Util.listsEqual(this.syntheticXyzSensor.payloadValuesZ(), 3.0, 6.0, 9.0));
    }

    @Test
    public void payloadValuesXMean() {
        assertEquals(1.0, this.syntheticXyzSensor.payloadValuesXMean(), 0.0);
    }

    @Test
    public void payloadValuesYMean() {
        assertEquals(2.0, this.syntheticXyzSensor.payloadValuesYMean(), 0.0);
    }

    @Test
    public void payloadValuesZMean() {
        assertEquals(3.0, this.syntheticXyzSensor.payloadValuesZMean(), 0.0);
    }

    @Test
    public void payloadValuesXMedian() {
        assertEquals(7.0, this.syntheticXyzSensor.payloadValuesXMedian(), 0.0);
    }

    @Test
    public void payloadValuesYMedian() {
        assertEquals(8.0, this.syntheticXyzSensor.payloadValuesYMedian(), 0.0);
    }

    @Test
    public void payloadValuesZMedian() {
        assertEquals(9.0, this.syntheticXyzSensor.payloadValuesZMedian(), 0.0);
    }

    @Test
    public void payloadValuesXStd() {
        assertEquals(4.0, this.syntheticXyzSensor.payloadValuesXStd(), 0.0);
    }

    @Test
    public void payloadValuesYStd() {
        assertEquals(5.0, this.syntheticXyzSensor.payloadValuesYStd(), 0.0);
    }

    @Test
    public void payloadValuesZStd() {
        assertEquals(6.0, this.syntheticXyzSensor.payloadValuesZStd(), 0.0);
    }
}