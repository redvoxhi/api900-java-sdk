package io.redvox.api900.tests;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class Resources {
    public static final String PACKET_1314 = "0000001314_1539627249223.rdvxz";
    public static final String MOCK_0 = "mock_0.json";

    private static ClassLoader classLoader = null;

    public static synchronized File loadResourceFile(final String resourceName) {
        if(classLoader == null) {
            classLoader = Resources.class.getClassLoader();
        }

        return new File(classLoader.getResource(resourceName).getFile());
    }

    public static synchronized Path loadResourcePath(final String resourcePath) {
        return loadResourceFile(resourcePath).toPath();
    }

    public static synchronized byte[] loadResourceBytes(final String resourceName) {
        try {
            return Files.readAllBytes(loadResourcePath(resourceName));
        } catch (IOException e) {
            e.printStackTrace();
            return new byte[0];
        }
    }

    public static String loadResourceJson(final String resourceName) {
        final byte[] bytes = loadResourceBytes(resourceName);
        return new String(bytes);
    }
}
