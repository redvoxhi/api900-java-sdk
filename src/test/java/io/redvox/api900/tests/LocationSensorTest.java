package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.sensors.LocationSensor;
import io.redvox.api900.sensors.TimeSynchronizationSensor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LocationSensorTest {
    private LocationSensor exampleSensor;
    private LocationSensor syntheticSensor;

    @Before
    public void setUp() {
        this.exampleSensor = Reader.wrap(
                Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314))
                        .get())
                .locationChannel()
                .get();
        this.syntheticSensor = Reader.wrap(
                Reader.readJson(Resources.loadResourceJson(Resources.MOCK_0))
                        .get())
                .locationChannel()
                .get();
    }

    @Test
    public void payloadValuesLatitude() {
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesLatitude(), 1.0, 6.0, 11.0));
    }

    @Test
    public void payloadValuesLongitude() {
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesLongitude(), 2.0, 7.0, 12.0));
    }

    @Test
    public void payloadValuesAltitude() {
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesAltitude(), 3.0, 8.0, 13.0));
    }

    @Test
    public void payloadValuesSpeed() {
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesSpeed(), 4.0, 9.0, 14.0));
    }

    @Test
    public void payloadValuesAccuracy() {
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValuesAccuracy(), 5.0, 10.0, 15.0));
    }

    @Test
    public void payloadValuesLatitudeMean() {
        assertEquals(1.0, this.syntheticSensor.payloadValuesLatitudeMean(), 0.0);
    }

    @Test
    public void payloadValuesLatitudeMedian() {
        assertEquals(11.0, this.syntheticSensor.payloadValuesLatitudeMedian(), 0.0);
    }

    @Test
    public void payloadValuesLatitudeStd() {
        assertEquals(6.0, this.syntheticSensor.payloadValuesLatitudeStd(), 0.0);
    }

    @Test
    public void payloadValuesLongitudeMean() {
        assertEquals(2.0, this.syntheticSensor.payloadValuesLongitudeMean(), 0.0);
    }

    @Test
    public void payloadValuesLongitudeMedian() {
        assertEquals(12.0, this.syntheticSensor.payloadValuesLongitudeMedian(), 0.0);
    }

    @Test
    public void payloadValuesLongitudeStd() {
        assertEquals(7.0, this.syntheticSensor.payloadValuesLongitudeStd(), 0.0);
    }

    @Test
    public void payloadValuesAltitudeMean() {
        assertEquals(3.0, this.syntheticSensor.payloadValuesAltitudeMean(), 0.0);
    }

    @Test
    public void payloadValuesAltitudeMedian() {
        assertEquals(13.0, this.syntheticSensor.payloadValuesAltitudeMedian(), 0.0);
    }

    @Test
    public void payloadValuesAltitudeStd() {
        assertEquals(8.0, this.syntheticSensor.payloadValuesAltitudeStd(), 0.0);
    }

    @Test
    public void payloadValuesSpeedMean() {
        assertEquals(4.0, this.syntheticSensor.payloadValuesSpeedMean(), 0.0);
    }

    @Test
    public void payloadValuesSpeedMedian() {
        assertEquals(14.0, this.syntheticSensor.payloadValuesSpeedMedian(), 0.0);
    }

    @Test
    public void payloadValuesSpeedStd() {
        assertEquals(9.0, this.syntheticSensor.payloadValuesSpeedStd(), 0.0);
    }

    @Test
    public void payloadValuesAccuracyMean() {
        assertEquals(5.0, this.syntheticSensor.payloadValuesAccuracyMean(), 0.0);
    }

    @Test
    public void payloadValuesAccuracyMedian() {
        assertEquals(15.0, this.syntheticSensor.payloadValuesAccuracyMedian(), 0.0);
    }

    @Test
    public void payloadValuesAccuracyStd() {
        assertEquals(10.0, this.syntheticSensor.payloadValuesAccuracyStd(), 0.0);
    }
}