package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.sensors.BarometerSensor;
import io.redvox.api900.sensors.LightSensor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LightSensorTest {
    private LightSensor exampleSensor;
    private LightSensor syntheticSensor;

    @Before
    public void setUp() throws Exception {
        this.exampleSensor = Reader.wrap(
                Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314))
                        .get())
                .lightChannel()
                .get();
        this.syntheticSensor = Reader.wrap(
                Reader.readJson(Resources.loadResourceJson(Resources.MOCK_0))
                        .get())
                .lightChannel()
                .get();
    }

    @Test
    public void testIsCorrectSensor() {
        assertEquals("TMD4903 Light Sensor", this.exampleSensor.sensorName());
        assertEquals("TMD4903 Light Sensor", this.syntheticSensor.sensorName());
    }
}