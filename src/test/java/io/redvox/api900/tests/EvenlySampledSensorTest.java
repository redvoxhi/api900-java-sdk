package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.WrappedRedvoxPacket;
import io.redvox.api900.sensors.EvenlySampledSensor;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Files;
import java.util.Arrays;

import static org.junit.Assert.*;

public class EvenlySampledSensorTest {
    private WrappedRedvoxPacket exampleRedvoxPacket;
    private WrappedRedvoxPacket syntheticPacket;
    private EvenlySampledSensor exampleEvenlySampledSensor;
    private EvenlySampledSensor syntheticEvenlySampledSensor;


    @Before
    public void setUp() throws Exception {
        this.exampleRedvoxPacket = Reader.wrap(Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314)).get());
        final String json = new String(Files.readAllBytes(Resources.loadResourcePath(Resources.MOCK_0)));
        this.syntheticPacket = Reader.wrap(Reader.readJson(json).get());
        this.exampleEvenlySampledSensor = this.exampleRedvoxPacket.microphoneChannel().get();
        this.syntheticEvenlySampledSensor = this.syntheticPacket.microphoneChannel().get();
    }

    @Test
    public void sensorRateHz() {
        assertEquals(80.0, this.exampleEvenlySampledSensor.sampleRateHz(), 0.01);
        assertEquals(800.0, this.syntheticEvenlySampledSensor.sampleRateHz(), 0.01);
    }

    @Test
    public void firstSampleTimestampEpochMicrosecondsUtc() {
        assertEquals(1539627249223992L, this.exampleEvenlySampledSensor.firstSampleTimestampEpochMicrosecondsUtc());
        assertEquals(20L, this.syntheticEvenlySampledSensor.firstSampleTimestampEpochMicrosecondsUtc());
    }

    @Test
    public void sensorName() {
        assertEquals("I/INTERNAL MIC", this.exampleEvenlySampledSensor.sensorName());
        assertEquals("test sensor name", this.syntheticEvenlySampledSensor.sensorName());
    }

    @Test
    public void payloadType() {
        assertEquals("INT32_PAYLOAD", this.exampleEvenlySampledSensor.payloadType());
        assertEquals("INT32_PAYLOAD", this.syntheticEvenlySampledSensor.payloadType());
    }

    @Test
    public void metadataList() {
        assertEquals(0, this.exampleEvenlySampledSensor.metadataList().size());
        assertEquals(Arrays.asList("a", "b", "c", "d"), this.syntheticEvenlySampledSensor.metadataList());
    }

    @Test
    public void metadataMap() {
        assertEquals(0, this.exampleEvenlySampledSensor.metadataMap().size());
        assertEquals("b", this.syntheticEvenlySampledSensor.metadataMap().get("a"));
        assertEquals("d", this.syntheticEvenlySampledSensor.metadataMap().get("c"));
    }

    @Test
    public void payloadValues() {
        assertEquals(4096, this.exampleEvenlySampledSensor.payloadValues().size());
        assertTrue(Util.listsEqual(this.syntheticEvenlySampledSensor.payloadValues(), 1L, 2L, 3L));
    }

    @Test
    public void payloadMean() {
        assertEquals(-124.48492032976355, this.exampleEvenlySampledSensor.payloadMean(), 0.01);
        assertEquals(1.0, this.syntheticEvenlySampledSensor.payloadMean(), 0.01);
    }

    @Test
    public void payloadMedian() {
        assertEquals(-124.0, this.exampleEvenlySampledSensor.payloadMedian(), 0.01);
        assertEquals(2.0, this.syntheticEvenlySampledSensor.payloadMedian(), 0.01);
    }

    @Test
    public void payloadStd() {
        assertEquals(225.98798345836425, this.exampleEvenlySampledSensor.payloadStd(), 0.001);
        assertEquals(2.0, this.syntheticEvenlySampledSensor.payloadStd(), 0.001);
    }
}