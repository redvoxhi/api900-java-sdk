package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.sensors.InfraredSensor;
import io.redvox.api900.sensors.LightSensor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InfraredSensorTest {
    private InfraredSensor exampleSensor;
    private InfraredSensor syntheticSensor;

    @Before
    public void setUp() throws Exception {
        this.exampleSensor = Reader.wrap(
                Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314))
                        .get())
                .infraredChannel()
                .get();
        this.syntheticSensor = Reader.wrap(
                Reader.readJson(Resources.loadResourceJson(Resources.MOCK_0))
                        .get())
                .infraredChannel()
                .get();
    }

    @Test
    public void testIsCorrectSensor() {
        assertEquals("TMD4903 Proximity Sensor", this.exampleSensor.sensorName());
        assertEquals("TMD4903 Proximity Sensor", this.syntheticSensor.sensorName());
    }
}