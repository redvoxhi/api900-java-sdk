package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.WrappedRedvoxPacket;
import io.redvox.apis.Api900.RedvoxPacket;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;

public class ReaderTest {

    @Test
    public void readFile() {
        final Optional<RedvoxPacket> redvoxPacketOptional =
                Reader.readFile(Resources.loadResourcePath(Resources.PACKET_1314));
        assertTrue(redvoxPacketOptional.isPresent());
        assertEquals(900, redvoxPacketOptional.get().getApi());
    }

    @Test
    public void readBuffer() {
        final byte[] buffer = Resources.loadResourceBytes(Resources.PACKET_1314);
        final Optional<RedvoxPacket> redvoxPacketOptional = Reader.readBuffer(buffer);
        assertTrue(redvoxPacketOptional.isPresent());
        assertEquals(900, redvoxPacketOptional.get().getApi());
    }

    @Test
    public void wrap() {
        final Optional<RedvoxPacket> redvoxPacketOptional =
                Reader.readFile(Resources.loadResourcePath(Resources.PACKET_1314));
        final WrappedRedvoxPacket wrappedRedvoxPacket = Reader.wrap(redvoxPacketOptional.get());
        assertEquals(900, wrappedRedvoxPacket.api());
    }
}