package io.redvox.api900.tests;

import io.redvox.api900.Reader;
import io.redvox.api900.sensors.TimeSynchronizationSensor;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TimeSynchronizationSensorTest {
    private TimeSynchronizationSensor exampleSensor;
    private TimeSynchronizationSensor syntheticSensor;

    @Before
    public void setUp() {
        this.exampleSensor = Reader.wrap(
                Reader.readBuffer(Resources.loadResourceBytes(Resources.PACKET_1314))
                        .get())
                .timeSynchronizationChannel()
                .get();
        this.syntheticSensor = Reader.wrap(
                Reader.readJson(Resources.loadResourceJson(Resources.MOCK_0))
                        .get())
                .timeSynchronizationChannel()
                .get();
    }

    @Test
    public void payloadType() {
        assertEquals("INT64_PAYLOAD", this.exampleSensor.payloadType());
        assertEquals("INT64_PAYLOAD", this.syntheticSensor.payloadType());
    }

    @Test
    public void payloadValues() {
        assertEquals(48, this.exampleSensor.payloadValues().size());
        assertTrue(Util.listsEqual(this.syntheticSensor.payloadValues(), 1L, 2L, 3L));
    }

    @Test
    public void metadataList() {
        assertEquals(0, this.exampleSensor.metadataList().size());
        assertTrue(Util.listsEqual(this.syntheticSensor.metadataList(), "a", "b"));
    }

    @Test
    public void metadataMap() {
        assertEquals(0, this.exampleSensor.metadataMap().size());
        assertEquals("b", this.syntheticSensor.metadataMap().get("a"));
    }
}