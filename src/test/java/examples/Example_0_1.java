package examples;

import io.redvox.api900.Reader;
import io.redvox.api900.WrappedRedvoxPacket;
import io.redvox.api900.sensors.*;
import io.redvox.api900.tests.Resources;
import io.redvox.apis.Api900;

import java.util.Optional;

public class Example_0_1 {
    public static void main(final String[] args) {
        // First, read in protobuf encoded RedVox file
        final Optional<Api900.RedvoxPacket> redvoxPacketOptional = Reader.readFile(Resources.loadResourcePath(Resources.PACKET_1314));

        // If we successfully loaded the protobuf encoded message, we can now wrap it to access high-level accessors.
        if(redvoxPacketOptional.isPresent()) {
            final WrappedRedvoxPacket wrappedRedvoxPacket = Reader.wrap(redvoxPacketOptional.get());

            // Print contents of top-level data fields
            System.out.println(wrappedRedvoxPacket.api());
            System.out.println(wrappedRedvoxPacket.redvoxId());
            System.out.println(wrappedRedvoxPacket.uuid());
            System.out.println(wrappedRedvoxPacket.authenticatedEmail());
            System.out.println(wrappedRedvoxPacket.authenticationToken());
            System.out.println(wrappedRedvoxPacket.firebaseToken());
            System.out.println(wrappedRedvoxPacket.isBackfilled());
            System.out.println(wrappedRedvoxPacket.isPrivate());
            System.out.println(wrappedRedvoxPacket.isScrambled());
            System.out.println(wrappedRedvoxPacket.deviceMake());
            System.out.println(wrappedRedvoxPacket.deviceModel());
            System.out.println(wrappedRedvoxPacket.deviceOs());
            System.out.println(wrappedRedvoxPacket.deviceOsVersion());
            System.out.println(wrappedRedvoxPacket.appVersion());
            System.out.println(wrappedRedvoxPacket.batteryLevelPercent());
            System.out.println(wrappedRedvoxPacket.deviceTemperatureC());
            System.out.println(wrappedRedvoxPacket.acquisitionServer());
            System.out.println(wrappedRedvoxPacket.timeSynchronizationChannel());
            System.out.println(wrappedRedvoxPacket.authenticationServer());
            System.out.println(wrappedRedvoxPacket.appFileStartTimestampEpochMicrosecondsUtc());
            System.out.println(wrappedRedvoxPacket.appFileStartTimestampMachine());
            System.out.println(wrappedRedvoxPacket.serverTimestampEpochMicrosecondsUtc());
            System.out.println(wrappedRedvoxPacket.metadataList());
            System.out.println(wrappedRedvoxPacket.metadataMap());

            // Retrieve and print contents of microphone sensor channel
            if(wrappedRedvoxPacket.hasMicrophoneChannel()){
                final MicrophoneSensor microphoneSensor = wrappedRedvoxPacket.microphoneChannel().get();
                System.out.println(microphoneSensor.sensorName());
                System.out.println(microphoneSensor.sampleRateHz());
                System.out.println(microphoneSensor.firstSampleTimestampEpochMicrosecondsUtc());
                System.out.println(microphoneSensor.payloadType());
                System.out.println(microphoneSensor.payloadValues());
                System.out.println(microphoneSensor.payloadMean());
                System.out.println(microphoneSensor.payloadMedian());
                System.out.println(microphoneSensor.payloadStd());
                System.out.println(microphoneSensor.metadataList());
                System.out.println(microphoneSensor.metadataMap());
            }

            // Retrieve and print contents of barometer sensor channel
            if(wrappedRedvoxPacket.hasBarometerChannel()) {
                final BarometerSensor barometerSensor = wrappedRedvoxPacket.barometerChannel().get();
                System.out.println(barometerSensor.sensorName());
                System.out.println(barometerSensor.timestampsMicrosecondsUtc());
                System.out.println(barometerSensor.sampleIntervalMean());
                System.out.println(barometerSensor.sampleIntervalMedian());
                System.out.println(barometerSensor.sampleIntervalStd());
                System.out.println(barometerSensor.payloadType());
                System.out.println(barometerSensor.payloadValues());
                System.out.println(barometerSensor.payloadMean());
                System.out.println(barometerSensor.payloadMedian());
                System.out.println(barometerSensor.payloadStd());
                System.out.println(barometerSensor.metadataList());
                System.out.println(barometerSensor.metadataMap());
            }

            // Retrieve and print contents of location sensor channel
            if(wrappedRedvoxPacket.hasLocationChannel()) {
                final LocationSensor locationSensor = wrappedRedvoxPacket.locationChannel().get();
                System.out.println(locationSensor.sensorName());
                System.out.println(locationSensor.timestampsMicrosecondsUtc());
                System.out.println(locationSensor.sampleIntervalMean());
                System.out.println(locationSensor.sampleIntervalMedian());
                System.out.println(locationSensor.sampleIntervalStd());
                System.out.println(locationSensor.metadataList());
                System.out.println(locationSensor.metadataMap());

                System.out.println(locationSensor.payloadType());
                System.out.println(locationSensor.payloadValues());

                System.out.println(locationSensor.payloadValuesLatitude());
                System.out.println(locationSensor.payloadValuesLatitudeMean());
                System.out.println(locationSensor.payloadValuesLatitudeMedian());
                System.out.println(locationSensor.payloadValuesLatitudeStd());

                System.out.println(locationSensor.payloadValuesLongitude());
                System.out.println(locationSensor.payloadValuesLongitudeMean());
                System.out.println(locationSensor.payloadValuesLongitudeMedian());
                System.out.println(locationSensor.payloadValuesLongitudeStd());

                System.out.println(locationSensor.payloadValuesAltitude());
                System.out.println(locationSensor.payloadValuesAltitudeMean());
                System.out.println(locationSensor.payloadValuesAltitudeMedian());
                System.out.println(locationSensor.payloadValuesAltitudeStd());

                System.out.println(locationSensor.payloadValuesSpeed());
                System.out.println(locationSensor.payloadValuesSpeedMean());
                System.out.println(locationSensor.payloadValuesSpeedMedian());
                System.out.println(locationSensor.payloadValuesSpeedStd());

                System.out.println(locationSensor.payloadValuesAccuracy());
                System.out.println(locationSensor.payloadValuesAccuracyMean());
                System.out.println(locationSensor.payloadValuesAccuracyMedian());
                System.out.println(locationSensor.payloadValuesAccuracyStd());
            }

            // Retrieve and print contents of time synchronization channel
            if(wrappedRedvoxPacket.hasTimeSynchronizationChannel()) {
                final TimeSynchronizationSensor timeSynchronizationSensor = wrappedRedvoxPacket.timeSynchronizationChannel().get();
                System.out.println(timeSynchronizationSensor.payloadType());
                System.out.println(timeSynchronizationSensor.payloadValues());
                System.out.println(timeSynchronizationSensor.metadataList());
                System.out.println(timeSynchronizationSensor.metadataMap());
            }

            // Retrieve and print contents of accelerometer channel
            if(wrappedRedvoxPacket.hasAccelerometerChannel()) {
                final AccelerometerSensor accelerometerSensor = wrappedRedvoxPacket.accelerometerChannel().get();
                System.out.println(accelerometerSensor.sensorName());
                System.out.println(accelerometerSensor.timestampsMicrosecondsUtc());
                System.out.println(accelerometerSensor.sampleIntervalMean());
                System.out.println(accelerometerSensor.sampleIntervalMedian());
                System.out.println(accelerometerSensor.sampleIntervalStd());
                System.out.println(accelerometerSensor.metadataList());
                System.out.println(accelerometerSensor.metadataMap());
                System.out.println(accelerometerSensor.payloadType());
                System.out.println(accelerometerSensor.payloadValues());
                System.out.println(accelerometerSensor.payloadValuesX());
                System.out.println(accelerometerSensor.payloadValuesY());
                System.out.println(accelerometerSensor.payloadValuesZ());
                System.out.println(accelerometerSensor.payloadValuesXMean());
                System.out.println(accelerometerSensor.payloadValuesXMedian());
                System.out.println(accelerometerSensor.payloadValuesXStd());
                System.out.println(accelerometerSensor.payloadValuesYMean());
                System.out.println(accelerometerSensor.payloadValuesYMedian());
                System.out.println(accelerometerSensor.payloadValuesYStd());
                System.out.println(accelerometerSensor.payloadValuesZMean());
                System.out.println(accelerometerSensor.payloadValuesZMedian());
                System.out.println(accelerometerSensor.payloadValuesZStd());
            }

            // Retrieve and print contents of magnetometer channel
            if(wrappedRedvoxPacket.hasMagnetometerChannel()) {
                final MagnetometerSensor magnetometerSensor = wrappedRedvoxPacket.magnetometerChannel().get();
                System.out.println(magnetometerSensor.sensorName());
                System.out.println(magnetometerSensor.timestampsMicrosecondsUtc());
                System.out.println(magnetometerSensor.sampleIntervalMean());
                System.out.println(magnetometerSensor.sampleIntervalMedian());
                System.out.println(magnetometerSensor.sampleIntervalStd());
                System.out.println(magnetometerSensor.metadataList());
                System.out.println(magnetometerSensor.metadataMap());
                System.out.println(magnetometerSensor.payloadType());
                System.out.println(magnetometerSensor.payloadValues());
                System.out.println(magnetometerSensor.payloadValuesX());
                System.out.println(magnetometerSensor.payloadValuesY());
                System.out.println(magnetometerSensor.payloadValuesZ());
                System.out.println(magnetometerSensor.payloadValuesXMean());
                System.out.println(magnetometerSensor.payloadValuesXMedian());
                System.out.println(magnetometerSensor.payloadValuesXStd());
                System.out.println(magnetometerSensor.payloadValuesYMean());
                System.out.println(magnetometerSensor.payloadValuesYMedian());
                System.out.println(magnetometerSensor.payloadValuesYStd());
                System.out.println(magnetometerSensor.payloadValuesZMean());
                System.out.println(magnetometerSensor.payloadValuesZMedian());
                System.out.println(magnetometerSensor.payloadValuesZStd());
            }

            // Retrieve and print contents of gyroscope channel
            if(wrappedRedvoxPacket.hasGyroscopeChannel()) {
                final GyroscopeSensor gyroscopeSensor = wrappedRedvoxPacket.gyroscopeChannel().get();
                System.out.println(gyroscopeSensor.sensorName());
                System.out.println(gyroscopeSensor.timestampsMicrosecondsUtc());
                System.out.println(gyroscopeSensor.sampleIntervalMean());
                System.out.println(gyroscopeSensor.sampleIntervalMedian());
                System.out.println(gyroscopeSensor.sampleIntervalStd());
                System.out.println(gyroscopeSensor.metadataList());
                System.out.println(gyroscopeSensor.metadataMap());
                System.out.println(gyroscopeSensor.payloadType());
                System.out.println(gyroscopeSensor.payloadValues());
                System.out.println(gyroscopeSensor.payloadValuesX());
                System.out.println(gyroscopeSensor.payloadValuesY());
                System.out.println(gyroscopeSensor.payloadValuesZ());
                System.out.println(gyroscopeSensor.payloadValuesXMean());
                System.out.println(gyroscopeSensor.payloadValuesXMedian());
                System.out.println(gyroscopeSensor.payloadValuesXStd());
                System.out.println(gyroscopeSensor.payloadValuesYMean());
                System.out.println(gyroscopeSensor.payloadValuesYMedian());
                System.out.println(gyroscopeSensor.payloadValuesYStd());
                System.out.println(gyroscopeSensor.payloadValuesZMean());
                System.out.println(gyroscopeSensor.payloadValuesZMedian());
                System.out.println(gyroscopeSensor.payloadValuesZStd());
            }

            // Retrieve and print contents of light sensor channel
            if(wrappedRedvoxPacket.hasLightChannel()) {
                final LightSensor lightSensor = wrappedRedvoxPacket.lightChannel().get();
                System.out.println(lightSensor.sensorName());
                System.out.println(lightSensor.timestampsMicrosecondsUtc());
                System.out.println(lightSensor.sampleIntervalMean());
                System.out.println(lightSensor.sampleIntervalMedian());
                System.out.println(lightSensor.sampleIntervalStd());
                System.out.println(lightSensor.payloadType());
                System.out.println(lightSensor.payloadValues());
                System.out.println(lightSensor.payloadMean());
                System.out.println(lightSensor.payloadMedian());
                System.out.println(lightSensor.payloadStd());
                System.out.println(lightSensor.metadataList());
                System.out.println(lightSensor.metadataMap());
            }

            // Retrieve and print contents of infrared sensor channel
            if(wrappedRedvoxPacket.hasInfraredChannel()) {
                final InfraredSensor infraredSensor = wrappedRedvoxPacket.infraredChannel().get();
                System.out.println(infraredSensor.sensorName());
                System.out.println(infraredSensor.timestampsMicrosecondsUtc());
                System.out.println(infraredSensor.sampleIntervalMean());
                System.out.println(infraredSensor.sampleIntervalMedian());
                System.out.println(infraredSensor.sampleIntervalStd());
                System.out.println(infraredSensor.payloadType());
                System.out.println(infraredSensor.payloadValues());
                System.out.println(infraredSensor.payloadMean());
                System.out.println(infraredSensor.payloadMedian());
                System.out.println(infraredSensor.payloadStd());
                System.out.println(infraredSensor.metadataList());
                System.out.println(infraredSensor.metadataMap());
            }
        }
    }
}
