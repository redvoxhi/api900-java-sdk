#!/bin/bash

VERSION=${1}
DIST=api900-java-sdk-source-${VERSION}

mkdir -p ${DIST}
cp -R src ${DIST}
cp -R docs ${DIST}
cp README.md ${DIST}

cp -R *.gradle ${DIST}
cp -r gradle ${DIST}
cp gradlew ${DIST}
cp gradlew.bat ${DIST}

tar cjf ${DIST}.tar.bz2 ${DIST}

rm -rf ${DIST}
